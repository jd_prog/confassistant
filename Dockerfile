FROM openjdk:8-jre-alpine
ADD target/confassistant.jar confassistant.jar
ADD src/main/resources/certificates certificates
ENTRYPOINT ["java", "-Dspring.profiles.active=docker", "-jar", "confassistant.jar"]