#!/usr/bin/env bash
timestamp=$(date "+%s")
echo "$timestamp"
mvn_clean="mvn clean"
mvn_package="mvn package"
docker_build="docker build -t yaroslavprokipchyn/confassistant-back-end ."
docker_tag="docker tag yaroslavprokipchyn/confassistant-back-end $timestamp"
eval $mvn_clean
eval $mvn_package
eval $docker_build
eval $docker_tag

