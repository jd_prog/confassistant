package com.qq.confassistant.unit.service;

import com.qq.confassistant.dao.StreamsDao;
import com.qq.confassistant.dao.TopicsDao;
import com.qq.confassistant.model.PublicUser;
import com.qq.confassistant.model.Stream;
import com.qq.confassistant.model.Topic;
import com.qq.confassistant.service.impl.TopicsService;
import com.qq.confassistant.validation.scenario.TopicValidationScenario;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.*;

/**
 * Created by yana on 19.04.18.
 */
@RunWith(MockitoJUnitRunner.class)
public class TopicsServiceUnitTest {

    @Mock
    private TopicsDao topicsDao;
    @Mock
    private StreamsDao streamsDao;
    @Mock
    private TopicValidationScenario topicValidationScenario;

    @InjectMocks
    private TopicsService topicsService;

    @Test
    public void get() {
        long fakeId = 8;
        Topic fakeTopic = new Topic();

        doNothing().when(topicValidationScenario).checkIfTopicExists(fakeId);
        when(topicsDao.findOne(fakeId)).thenReturn(fakeTopic);

        assertThat(topicsService.get(fakeId),
                equalTo(fakeTopic));
    }

    @Test
    public void create() {
        long streamId = 5;

        Topic fakeTopicToCreate = new Topic();
        List<PublicUser> fakeSpeakers = new ArrayList<>();
        PublicUser fakeSpeaker = new PublicUser();
        fakeSpeakers.add(fakeSpeaker);
        fakeTopicToCreate.setUsers(fakeSpeakers);

        Topic fakeTopic = new Topic();

        doNothing().when(topicValidationScenario).checkIfStreamExists(streamId);

        when(topicsDao.save(fakeTopicToCreate)).thenReturn(fakeTopic);

        assertThat(topicsService.create(streamId, fakeTopicToCreate), equalTo(fakeTopic));
        assertThat(fakeTopicToCreate.getUsers(), equalTo(fakeSpeakers));
    }

    @Test
    public void update() {
        long fakeStreamId = 5;
        long fakeTopicId = 5;
        Topic fakeTopicToUpdate = new Topic();
        List<PublicUser> fakeSpeakers = new ArrayList<>();
        fakeTopicToUpdate.setUsers(fakeSpeakers);

        Topic fakeTopic = new Topic();

        doNothing().when(topicValidationScenario).checkIfCanBeUpdated(fakeStreamId, fakeTopicId);
        when(topicsDao.save(fakeTopicToUpdate)).thenReturn(fakeTopic);

        assertThat(topicsService.update(fakeStreamId, fakeTopicToUpdate),
                equalTo(fakeTopic));
    }

    @Test
    public void delete() {
        long fakeId = 4;

        doNothing().when(topicValidationScenario).checkIfTopicExists(fakeId);
        doNothing().when(topicsDao).delete(fakeId);
        topicsService.delete(fakeId);
        verify(topicsDao, times(1)).delete(fakeId);
    }


    @Test
    public void getTopicsByStream() {
        long fakeStreamId = 1;
        Stream fakeStream = new Stream();
        List<Topic> fakeTopicListByStream = new ArrayList<>();

        doNothing().when(topicValidationScenario).checkIfStreamExists(fakeStreamId);
        when(streamsDao.findOne(fakeStreamId)).thenReturn(fakeStream);
        when(topicsDao.findTopicsByStream(fakeStream)).thenReturn(fakeTopicListByStream);

        assertThat(topicsService.getAllByStreamId(fakeStreamId), equalTo(fakeTopicListByStream));
    }

}
