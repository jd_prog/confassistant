package com.qq.confassistant.unit.service;

import com.qq.confassistant.dao.PublicUserDao;
import com.qq.confassistant.dao.QuestionsDao;
import com.qq.confassistant.dao.TopicsDao;
import com.qq.confassistant.dao.VotesDao;
import com.qq.confassistant.model.*;
import com.qq.confassistant.service.IUserService;
import com.qq.confassistant.service.impl.QuestionsService;
import com.qq.confassistant.validation.scenario.QuestionValidationScenario;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.*;

/**
 * Created by yana on 19.04.18.
 */
@RunWith(MockitoJUnitRunner.class)
public class QuestionsServiceTest {

    @Mock
    private QuestionsDao questionsDao;
    @Mock
    private TopicsDao topicsDao;
    @Mock
    private VotesDao votesDao;
    @Mock
    private PublicUserDao userDao;
    @Mock
    private QuestionValidationScenario questionValidationScenario;

    @InjectMocks
    private QuestionsService questionsService;

    @Test
    public void getAll() {

        long fakeTopicId = 5;
        List<Question> fakeQuestions = new ArrayList<>();

        when(questionsDao.findAll()).thenReturn(fakeQuestions);

        assertThat(questionsService.getAllByTopic(fakeTopicId),
                equalTo(fakeQuestions));
    }

    @Test
    public void get() {
        long fakeId = 6;
        Question fakeQuestion = new Question();

        when(questionsDao.findOne(fakeId)).thenReturn(fakeQuestion);

        assertThat(questionsService.get(fakeId),
                equalTo(fakeQuestion));
    }

    @Test
    public void create() {
        long fakeTopicId = 8;
        Question fakeQuestionToCreate = new Question();
        Question fakeQuestion = new Question();
        Topic fakeTopic = new Topic();

        String fakeUsername = "fake.user@mail.com";
        PublicUser fakeUser = new PublicUser();

        doNothing().when(questionValidationScenario).checkIfCanBeCreated(fakeTopicId);
        when(userDao.findOne(fakeUsername)).thenReturn(fakeUser);

        when(topicsDao.findOne(fakeTopicId)).thenReturn(fakeTopic);
        when(questionsDao.save(fakeQuestionToCreate)).thenReturn(fakeQuestion);

        assertThat(questionsService.create(fakeTopicId, fakeUsername, fakeQuestionToCreate),
                equalTo(fakeQuestion));
    }

    @Test
    public void updateWithZeroCountOfVotes() {
        long fakeTopicId = 7;
        long fakeQuestionId = 6;

        Question oldFakeQuestion = new Question();

        Question fakeQuestionToUpdate = new Question();
        fakeQuestionToUpdate.setId(fakeQuestionId);

        Question fakeQuestion = new Question();

        when(questionsDao.findOne(fakeQuestionId)).thenReturn(oldFakeQuestion);
        when(questionsDao.save(fakeQuestionToUpdate)).thenReturn(fakeQuestion);

        assertThat(
                questionsService.update(fakeTopicId, fakeQuestionToUpdate),
                equalTo(fakeQuestion));
    }

    @Test
    public void voteFirstTime() {

        long fakeQuestionId = 6;
        int fakeVotesCount = 0;
        String fakeUserName = "jon.snow@mail.com";

        Question fakeQuestion = new Question();
        Vote fakeVote = new Vote();

        when(questionsDao.findOne(fakeQuestionId)).thenReturn(fakeQuestion);
        when(votesDao.countAllByQuestionIdAndUsername(fakeQuestionId, fakeUserName)).thenReturn(fakeVotesCount);
        when(votesDao.save(fakeVote)).thenReturn(fakeVote);
        doNothing().when(questionValidationScenario).checkIfCanBeVoted(fakeQuestionId, fakeUserName);

        assertThat(
                questionsService.vote(fakeQuestionId, fakeUserName),
                equalTo(fakeQuestion));
    }


    @Test(expected = IllegalAccessException.class)
    public void voteSecondTime() {

        long fakeQuestionId = 6;
        String fakeUserName = "jon.snow@mail.com";

        Question fakeQuestion = new Question();
        Vote fakeVote = new Vote();

        doThrow(IllegalAccessException.class).when(questionValidationScenario).checkIfCanBeVoted(fakeQuestionId, fakeUserName);

        when(questionsDao.findOne(fakeQuestionId)).thenReturn(fakeQuestion);
        when(votesDao.save(fakeVote)).thenReturn(fakeVote);

        assertThat(
                questionsService.vote(fakeQuestionId, fakeUserName),
                equalTo(fakeQuestion));
    }

    @Test
    public void delete() {
        long fakeId = 4;

        doNothing().when(questionValidationScenario).checkIfExists(fakeId);
        doNothing().when(questionsDao).delete(fakeId);

        questionsService.delete(fakeId);

        verify(questionsDao, times(1)).delete(fakeId);
    }

    @Test
    public void changeStatus(){

        long fakeQuestionId = 5;
        Question fakeQuestionToUpdate = new Question();
        Question fakeQuestion = new Question();
        QuestionStatus fakeQuestionStatus = QuestionStatus.OPENED;

        doNothing().when(questionValidationScenario).checkIfExists(fakeQuestionId);
        when(questionsDao.findOne(fakeQuestionId)).thenReturn(fakeQuestionToUpdate);
        when(questionsDao.save(fakeQuestionToUpdate)).thenReturn(fakeQuestion);

        assertThat(questionsService.changeStatus(fakeQuestionId, fakeQuestionStatus),
                equalTo(fakeQuestion));
    }

}
