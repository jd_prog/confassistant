package com.qq.confassistant.unit.service;

import com.qq.confassistant.dao.EventsDao;
import com.qq.confassistant.dao.StreamsDao;
import com.qq.confassistant.model.Event;
import com.qq.confassistant.model.Stream;
import com.qq.confassistant.service.impl.StreamsService;
import com.qq.confassistant.validation.scenario.StreamValidationScenario;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.*;

/**
 * Created by yana on 18.04.18.
 */
@RunWith(MockitoJUnitRunner.class)
public class StreamsServiceTest {

    @Mock
    private StreamsDao streamsDao;
    @Mock
    private EventsDao eventsDao;
    @Mock
    private StreamValidationScenario streamValidationScenario;

    @InjectMocks
    private StreamsService streamsService;


    @Test
    public void getStreamsByEventId(){
        long eventId=1;
        List<Stream> fakeStreamsByEventId = new ArrayList<>();
        Event fakeEvent = new Event();
        when(eventsDao.findOne(eventId)).thenReturn(fakeEvent);
        when(streamsDao.findStreamsByEvent(fakeEvent)).thenReturn(fakeStreamsByEventId);
        assertThat(streamsService.getStreamsByEvent(eventId), equalTo(fakeStreamsByEventId));
    }

    @Test
    public void getAllStreams(){

        List<Stream> fakeStreams = new ArrayList<>();

        when(streamsDao.findAll()).thenReturn(fakeStreams);

        assertThat(streamsService.getAll(), equalTo(fakeStreams));

    }

    @Test
    public void getStreamGetById(){
        long fakeStreamId = 8;
        Stream fakeStream = new Stream();

        when(streamsDao.findOne(fakeStreamId)).thenReturn(fakeStream);

        assertThat(streamsService.get(fakeStreamId), equalTo(fakeStream));

    }

    @Test
    public void createStream(){
        long fakeEventId = 4;
        Stream fakeStreamToSave = new Stream();
        Stream fakeStream = new Stream();
        Event fakeEvent = new Event();

        when(eventsDao.findOne(fakeEventId)).thenReturn(fakeEvent);
        when(streamsDao.save(fakeStreamToSave)).thenReturn(fakeStream);

        assertThat(streamsService.create(fakeEventId, fakeStreamToSave), equalTo(fakeStream));
    }


    @Test
    public void updateStream(){

        long streamId = 4;
        Stream fakeStreamToUpdate = mock(Stream.class);
        Stream fakeStream = mock(Stream.class);

        doNothing().when(streamValidationScenario).checkIfStreamExists(streamId);
        when(streamsDao.save(fakeStreamToUpdate)).thenReturn(fakeStream);

        assertThat(streamsService.update(fakeStreamToUpdate), equalTo(fakeStream));
    }

    @Test
    public void deleteStream(){
        long fakeId = 6;

        doNothing().when(streamValidationScenario).checkIfStreamExists(fakeId);
        doNothing().when(streamsDao).delete(fakeId);

        streamsService.delete(fakeId);

        verify(streamsDao, times(1)).delete(fakeId);

    }

}
