package com.qq.confassistant.unit.service;

import com.qq.confassistant.dao.EventsDao;
import com.qq.confassistant.exception.EntityNotFoundException;
import com.qq.confassistant.model.*;
import com.qq.confassistant.service.IActualVisitorsService;
import com.qq.confassistant.service.IEventSharedUsersService;
import com.qq.confassistant.service.IPotentialVisitorsService;
import com.qq.confassistant.service.impl.EventService;
import com.qq.confassistant.service.impl.StreamsService;
import com.qq.confassistant.service.impl.TagService;
import com.qq.confassistant.validation.scenario.EventValidationScenario;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static java.util.stream.Stream.of;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.*;

/**
 * Created by yana on 17.04.18.
 */
@RunWith(MockitoJUnitRunner.class)
public class EventsServiceUnitTests {

    @Mock
    private EventsDao eventsDao;
    @Mock
    private StreamsService streamsService;
    @Mock
    private EventValidationScenario validationScenario;
    @Mock
    private IActualVisitorsService actualVisitorsService;
    @Mock
    private IPotentialVisitorsService potentialVisitorsService;
    @Mock
    private IEventSharedUsersService eventSharedUsersService;

    @Mock
    private TagService tagService;

    @Spy
    @InjectMocks
    private EventService eventService;

    @Test
    public void getById() {

        long id = 5L;

        Event fakeEvent = new Event();

        doNothing().when(validationScenario).checkIfExists(id);
        when(eventsDao.findOne(id)).thenReturn(fakeEvent);

        Event event = eventService.get(id);

        assertThat(event, equalTo(fakeEvent));
    }

    @Test(expected = EntityNotFoundException.class)
    public void getByNotExistingId() {

        long id = -1L;

        doThrow(EntityNotFoundException.class).when(validationScenario).checkIfExists(id);

        eventService.get(id);
    }

    @Test
    public void getAll() {

        List<Event> fakeEvents = new ArrayList<>();

        when(eventsDao.findAll()).thenReturn(fakeEvents);

        assertThat(eventService.getAll(), equalTo(fakeEvents));
    }

    @Test
    public void getPublished() {
        List<Event> fakeEvents = new ArrayList<>();
        when(eventsDao.findEventsByStatus(EventStatus.PUBLISH)).thenReturn(fakeEvents);
        assertThat(eventService.getPublished(), equalTo(fakeEvents));
    }

    @Test
    public void createEventTest() {

        Event fakeEventToCreate = new Event();
        Location fakeLocationToCreate = new Location();
        fakeEventToCreate.setLocation(fakeLocationToCreate);

        Event fakeEvent = new Event();
        Location fakeLocation = new Location();
        fakeEvent.setLocation(fakeLocation);

        when(eventsDao.save(fakeEventToCreate)).thenReturn(fakeEvent);

        Event event = eventService.create(fakeEventToCreate);

        assertThat(event, equalTo(fakeEvent));
        assertThat(event.getLocation(), equalTo(fakeLocation));
    }

    @Test
    public void updateEventTest() {

        long fakeEventId = 7;

        Event fakeEventToCreate = new Event();
        Location fakeLocationToCreate = new Location();
        fakeEventToCreate.setLocation(fakeLocationToCreate);

        Event fakeEvent = new Event();
        Location fakeLocation = new Location();
        fakeEvent.setLocation(fakeLocation);

        List<Stream> fakeStreams = new ArrayList<>();

        when(streamsService.getStreamsByEvent(fakeEventId)).thenReturn(fakeStreams);
        when(eventsDao.save(fakeEventToCreate)).thenReturn(fakeEvent);

        Event event = eventService.update(fakeEventToCreate);

        assertThat(event, equalTo(fakeEvent));
        assertThat(event.getLocation(), equalTo(fakeLocation));
    }

    @Test
    public void changeStatusToPublishTest() {
        long fakeEventId = 7;
        Event fakeEvent = new Event();
        when(eventsDao.findOne(fakeEventId)).thenReturn(fakeEvent);
        when(eventsDao.save(eventsDao.findOne(fakeEventId))).thenReturn(fakeEvent);
        Event event = eventService.changeStatusToPublish(fakeEventId);
        assertThat(event, equalTo(fakeEvent));
    }

    @Test
    public void deleteEvent() {

        long fakeId = 4;
        Event fakeEvent = new Event();
        fakeEvent.setId(fakeId);
        when(eventsDao.findOne(fakeId)).thenReturn(fakeEvent);
        doNothing().when(validationScenario).checkIfExists(fakeId);
        doNothing().when(eventSharedUsersService).removeEventSharedUsers(fakeEvent);
        doNothing().when(actualVisitorsService).removeEventActualVisitors(fakeEvent);
        doNothing().when(potentialVisitorsService).removeEventPotentialVisitors(fakeEvent);
        doNothing().when(eventsDao).delete(fakeId);

        eventService.delete(fakeId);

        verify(eventsDao, times(1)).delete(fakeId);
    }

    @Test(expected = EntityNotFoundException.class)
    public void deleteEventThatDoesNotExists() {

        long fakeId = 4;
        Event fakeEvent = new Event();
        fakeEvent.setId(fakeId);
        when(eventsDao.findOne(fakeId)).thenReturn(fakeEvent);
        doThrow(EntityNotFoundException.class).when(validationScenario).checkIfExists(fakeId);
        doNothing().when(eventSharedUsersService).removeEventSharedUsers(fakeEvent);
        doNothing().when(actualVisitorsService).removeEventActualVisitors(fakeEvent);
        doNothing().when(potentialVisitorsService).removeEventPotentialVisitors(fakeEvent);
        doNothing().when(eventsDao).delete(fakeId);

        eventService.delete(fakeId);

        verify(eventsDao, times(0)).delete(fakeId);
    }

    @Test
    public void findByTagIdTest() {
        Long fakeTagId = 1L;
        Set<Event> fakeEvents=new HashSet<Event>(){{
            add(new Event());
            add(new Event());
            add(new Event());
        }};
        Tag fakeTag = new Tag(1L, "name", new ArrayList<>());
        when(tagService.getById(fakeTagId)).thenReturn(fakeTag);
        Set<Tag> fakeAllPossibleTags = new HashSet<>();
        when(tagService.findAllInTag(fakeTag)).thenReturn(fakeAllPossibleTags);
        assertThat(fakeAllPossibleTags, equalTo(tagService.findAllInTag(fakeTag)));
        doReturn(fakeEvents).when(eventService).getAllByTags(fakeAllPossibleTags);
        Set<Event> fakeReceivedEvents = eventService.findByTagId(fakeTagId);
        assertThat(fakeReceivedEvents, equalTo(fakeEvents));

    }

    @Test
    public void getAllByTagTest() {
        Event event1 = new Event();
        event1.setStatus(EventStatus.PUBLISH);
        Event event2 = new Event();
        event2.setStatus(EventStatus.PUBLISH);
        ArrayList<Event> fakePublishEvents = new ArrayList<Event>() {{
            add(event1);
            add(event2);
        }};
        List<Event> fakeEvents = new ArrayList<Event>() {{
            add(new Event());
            add(new Event());
            addAll(fakePublishEvents);
        }};
        Tag fakeTag = new Tag(1L, "name", new ArrayList<>());
        when(eventsDao.findAllByTags(fakeTag)).thenReturn(fakeEvents);
        List<Event> fakeReceivedEvents = eventService.getAllByTag(fakeTag);
        assertThat(fakeReceivedEvents, equalTo(fakePublishEvents));
    }
 @Test
    public void getAllByTagsTest() {
        Event event1 = new Event();
        event1.setStatus(EventStatus.PUBLISH);
        Event event2 = new Event();
        event2.setStatus(EventStatus.PUBLISH);
        Set<Event> fakePublishEvents = new HashSet<Event>() {{
            add(event1);
            add(event2);
        }};
        List<Event> fakeEvents = new ArrayList<Event>() {{
            add(new Event());
            add(new Event());
            addAll(fakePublishEvents);
        }};
        Set<Tag> fakeTags = new HashSet<>();
        Tag fakeTag = new Tag(1L, "name", new ArrayList<>());
        fakeTags.add(fakeTag);
        when(eventsDao.findAllByTags(fakeTag)).thenReturn(fakeEvents);
        Set<Event> fakeReceivedEvents = eventService.getAllByTags(fakeTags);
        assertThat(fakeReceivedEvents, equalTo(fakePublishEvents));
    }

    @Test
    public void findByTagNameInTest() {
        Tag fakeTag = new Tag(1L, "name", new ArrayList<>());
        List<Event> fakeEvents = new ArrayList<>();
        String searchString = "fakeString";
        when(tagService.findByOneTagNameIn(searchString)).thenReturn(fakeTag);
        doReturn(fakeEvents).when(eventService).getAllByTag(fakeTag);
        List<Event> fakeReceivedEvents = eventService.findByTagNameIn(searchString);
        assertThat(fakeEvents, equalTo(fakeReceivedEvents));
    }

    @Test
    public void findByTextWithTagsTest() {
        Set<Tag> fakeTags = new HashSet<Tag>() {{
            add(new Tag());
        }};
        Set<Event> fakeEvents = of(new Event(), new Event()).collect(Collectors.toSet());


        String searchString = "fakeString";
        when(tagService.findByNameIn(searchString)).thenReturn(fakeTags);
        doReturn(fakeEvents).when(eventService).getAllByTags(fakeTags);


        Set<Event> fakeReceivedEvents = eventService.findByTextWithTags(searchString);

        verify(tagService).findByNameIn(searchString);
        assertThat(fakeReceivedEvents, equalTo(fakeEvents));
    }


}