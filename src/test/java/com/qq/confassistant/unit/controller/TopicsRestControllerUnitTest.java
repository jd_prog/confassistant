package com.qq.confassistant.unit.controller;

import com.qq.confassistant.conroller.TopicsRestController;
import com.qq.confassistant.model.Topic;
import com.qq.confassistant.service.ITopicsService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.*;

/**
 * Created by yana on 19.04.18.
 */
@RunWith(MockitoJUnitRunner.class)
public class TopicsRestControllerUnitTest {

    @Mock
    private ITopicsService topicsService;

    @InjectMocks
    private TopicsRestController topicsRestController;

    @Test
    public void getAll() {
        long fakeStreamId = 4;
        List<Topic> fakeTopics = new ArrayList<>();

        when(topicsService.getAllByStreamId(fakeStreamId))
                .thenReturn(fakeTopics);

        assertThat(topicsRestController.getTopicsByStreamId(fakeStreamId),
                equalTo(fakeTopics));
    }

    @Test
    public void get() {
        long fakeTopicId = 4;
        Topic fakeTopic = new Topic();
        when(topicsService.get(fakeTopicId))
                .thenReturn(fakeTopic);

        assertThat(topicsRestController.get(fakeTopicId),
                equalTo(fakeTopic));
    }

    @Test
    public void create() {

        long fakeEventId = 8;
        Topic fakeTopicToCreate = new Topic();
        Topic fakeTopic = new Topic();

        when(topicsService.create(fakeEventId, fakeTopicToCreate))
                .thenReturn(fakeTopic);

        assertThat(topicsRestController.create(fakeEventId, fakeTopicToCreate),
                equalTo(fakeTopic));

    }

    @Test
    public void update() {
        long fakeEventId = 8;
        Topic fakeTopicToUpdate = new Topic();
        Topic fakeTopic = new Topic();

        when(topicsService.update(fakeEventId, fakeTopicToUpdate))
                .thenReturn(fakeTopic);

        assertThat(topicsRestController.update(fakeEventId, fakeTopicToUpdate),
                equalTo(fakeTopic));
    }

    @Test
    public void delete() {
        long fakeId = 7;
        doNothing().when(topicsService).delete(fakeId);

        topicsRestController.delete(fakeId);

        verify(topicsService, times(1)).delete(fakeId);
    }
}
