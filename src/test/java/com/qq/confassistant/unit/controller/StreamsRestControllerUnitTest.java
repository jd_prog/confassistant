package com.qq.confassistant.unit.controller;

import com.qq.confassistant.conroller.StreamsRestController;
import com.qq.confassistant.dao.StreamsDao;
import com.qq.confassistant.model.Event;
import com.qq.confassistant.model.Stream;
import com.qq.confassistant.model.Topic;
import com.qq.confassistant.service.IStreamsService;
import com.qq.confassistant.service.impl.StreamsService;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.*;

/**
 * Created by yana on 19.04.18.
 */
@RunWith(MockitoJUnitRunner.class)
public class StreamsRestControllerUnitTest {

    @Mock
    private IStreamsService streamsService;

    @InjectMocks
    private StreamsRestController streamsRestController;




    @Test
    public void getAll(){
        List<Stream> fakeStreams = new ArrayList<>();
        long fakeEventId = 7;

        when(streamsService.getAll()).thenReturn(fakeStreams);

        assertThat(streamsRestController.getStreamsByEvent(fakeEventId),
                equalTo(fakeStreams));
    }

    @Test
    public void get(){
        long fakeId = 7;
        Stream fakeStream = new Stream();

        when(streamsService.get(fakeId)).thenReturn(fakeStream);

        assertThat(streamsRestController.get(fakeId),
                equalTo(fakeStream));
    }

    @Test
    public void create(){
        long fakeEventId = 8;
        Stream fakeStreamToCreate = new Stream();
        Stream fakeStream = new Stream();

        when(streamsService.create(fakeEventId, fakeStreamToCreate))
                .thenReturn(fakeStream);
        assertThat(streamsRestController.create(fakeEventId, fakeStreamToCreate),
                equalTo(fakeStream));
    }


    @Test
    public void testGetStreamsByEventId() throws Exception {
       long eventId = 1;
       List<Stream> fakeStreamsByEventId = new ArrayList<>();
       when(streamsService.getStreamsByEvent(eventId)).thenReturn(fakeStreamsByEventId);
       assertThat(streamsRestController.getStreamsByEvent(eventId), equalTo(fakeStreamsByEventId));
    }


    @Test
    public void update(){
        Stream fakeStreamToUpdate = new Stream();
        Stream fakeStream = new Stream();

        when(streamsService.update(fakeStreamToUpdate))
                .thenReturn(fakeStream);

        assertThat(streamsRestController.update(fakeStreamToUpdate),
                equalTo(fakeStream));
    }

    @Test
    public void delete(){
        long fakeId = 5;
        doNothing().when(streamsService).delete(fakeId);

        streamsRestController.delete(fakeId);

        verify(streamsService, times(1)).delete(fakeId);
    }



}
