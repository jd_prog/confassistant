package com.qq.confassistant.unit.controller;

import com.qq.confassistant.conroller.EventsRestController;
import com.qq.confassistant.model.Event;
import com.qq.confassistant.service.IEventService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.*;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.mockito.Mockito.*;

/**
 * Created by yana on 19.04.18.
 */
@RunWith(MockitoJUnitRunner.class)
public class EventsRestControllerUnitTest {

    @Mock
    private IEventService eventService;

    @InjectMocks
    private EventsRestController eventsRestController;

    @Test
    public void getPublishedEvents() {

        List<Event> fakeEvents = new ArrayList<>();

        when(eventService.getPublished()).thenReturn(fakeEvents);

        assertThat(eventsRestController.getPublishedEvents(), equalTo(fakeEvents));
    }

    @Test
    public void getEvent() {

        long fakeId = 8;
        Event fakeEvent = new Event();

        when(eventService.get(fakeId)).thenReturn(fakeEvent);

        Event event = eventsRestController.getEvent(fakeId);

        assertThat(event, equalTo(fakeEvent));
    }

    @Test
    public void createEvent() {

        Event fakeEventToCreate = new Event();
        Event fakeEvent = new Event();

        when(eventService.create(fakeEventToCreate)).thenReturn(fakeEvent);

        Event event = eventsRestController.create(fakeEventToCreate);

        assertThat(event, equalTo(fakeEvent));
    }

    @Test
    public void delete() {

        long fakeId = 8;
        doNothing().when(eventService).delete(fakeId);

        eventsRestController.delete(fakeId);

        verify(eventService, times(1)).delete(fakeId);
    }

    @Test
    public void update() {

        Event fakeEventToUpdate = new Event();
        Event fakeEvent = new Event();

        when(eventService.update(fakeEventToUpdate)).thenReturn(fakeEvent);

        Event event = eventsRestController.update(fakeEventToUpdate);

        assertThat(event, equalTo(fakeEvent));
    }

    @Test
    public void changeStatus() {
        Event fakeEvent = new Event();
        long fakeId = 8;
        when(eventService.changeStatusToPublish(fakeId)).thenReturn(fakeEvent);
        Event event = eventsRestController.changeStatus(fakeId);
        assertThat(event, equalTo(fakeEvent));
    }

    @Test
    public void getByTagId() {
        Set<Event> fakeEvents = new HashSet<Event>() {{
            add(new Event());
            add(new Event());
        }};
        Long tagId =1L;
        when(eventService.findByTagId(tagId)).thenReturn(fakeEvents);
        Set<Event> eventsByTagId = eventsRestController.getByTagId(tagId);
        assertThat(eventsByTagId, equalTo(fakeEvents));
    }

    @Test
    public void getByTextWithTags() {
        Set<Event> fakeEvents = new HashSet<Event>() {{
            add(new Event());
            add(new Event());
        }};
        String fakeText="Fake text";
        when(eventService.findByTextWithTags(fakeText)).thenReturn(fakeEvents);
        Set<Event> eventsByText = eventsRestController.getByTextWithTags(fakeText);
        assertThat(eventsByText, equalTo(fakeEvents));

    }
    @Test
    public void getByTagNameIn() {
        List<Event> fakeEvents = new ArrayList<Event>() {{
            add(new Event());
            add(new Event());
        }};
        String fakeTag="fakeTag";
        when(eventService.findByTagNameIn(fakeTag)).thenReturn(fakeEvents);
        List<Event> eventsByTag = eventsRestController.getByTagNameIn(fakeTag);
        assertThat(eventsByTag, equalTo(fakeEvents));

    }
}
