package com.qq.confassistant.unit.controller;

import com.qq.confassistant.conroller.QuestionsRestController;
import com.qq.confassistant.model.Question;
import com.qq.confassistant.model.QuestionStatus;
import com.qq.confassistant.service.IQuestionsService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.*;

/**
 * Created by yana on 19.04.18.
 */
@RunWith(MockitoJUnitRunner.class)
public class QuestionsRestControllerUnitTest {

    @Mock
    private IQuestionsService questionsService;

    @InjectMocks
    private QuestionsRestController questionsRestController;

    @Test
    public void getAll() {

        List<Question> fakeQuestions = new ArrayList<>();
        long fakeTopicId = 8;

        when(questionsService.getAllByTopic(fakeTopicId)).thenReturn(fakeQuestions);

        assertThat(questionsRestController.getAllByTopic(fakeTopicId), equalTo(fakeQuestions));
    }



    @Test
    public void get() {
        long fakeId = 7;
        Question fakeQuestion = new Question();

        when(questionsService.get(fakeId)).thenReturn(fakeQuestion);

        Question question = questionsRestController.get(fakeId);

        assertThat(question, equalTo(fakeQuestion));
    }

    @Test
    public void create() {

        long fakeTopicId = 7;
        Question fakeQuestionToCreate= new Question();
        Question fakeQuestion = new Question();
        Principal principal = mock(Principal.class);

        String fakeUsername = "fake.user@mail.com";

        when(principal.getName()).thenReturn(fakeUsername);
        when(questionsService.create(fakeTopicId, fakeUsername, fakeQuestionToCreate))
                .thenReturn(fakeQuestion);

        Question question = questionsRestController.create(fakeTopicId,  fakeQuestionToCreate, principal);

        assertThat(question, equalTo(fakeQuestion));
    }

    @Test
    public void update() {
        long fakeTopicId = 7;
        Question fakeQuestionToUpdate = new Question();
        Question fakeQuestion = new Question();

        when(questionsService.update(fakeTopicId, fakeQuestionToUpdate))
                .thenReturn(fakeQuestion);

        Question question = questionsRestController.update(fakeTopicId, fakeQuestionToUpdate);

        assertThat(question, equalTo(fakeQuestion));    }

    @Test
    public void open(){

        long fakeQuestionId = 4;
        QuestionStatus status = QuestionStatus.OPENED;
        Question fakeQuestion = new Question();

        when(questionsService.changeStatus(fakeQuestionId, status))
                .thenReturn(fakeQuestion);

        Question question = questionsRestController.open(fakeQuestionId);

        assertThat(question, equalTo(fakeQuestion));
    }


    @Test
    public void close(){
        long fakeQuestionId = 4;
        QuestionStatus status = QuestionStatus.CLOSED;
        Question fakeQuestion = new Question();

        when(questionsService.changeStatus(fakeQuestionId, status))
                .thenReturn(fakeQuestion);

        Question question = questionsRestController.close(fakeQuestionId);

        assertThat(question, equalTo(fakeQuestion));    }

    @Test
    public void vote(){

        long fakeQuestionId = 4;
        String fakeUserName = "jon.smith@mail.com";
        Question fakeQuestion = new Question();
        Principal fakePrincipal = mock(Principal.class);

        when(fakePrincipal.getName()).thenReturn(fakeUserName);

        when(questionsService.vote(fakeQuestionId, fakeUserName))
                .thenReturn(fakeQuestion);

        Question question = questionsRestController.vote( fakeQuestionId, fakePrincipal);

        assertThat(question, equalTo(fakeQuestion));
    }


    @Test
    public void delete() {

        long fakeId = 8;
        doNothing().when(questionsService).delete(fakeId);

        questionsRestController.delete(fakeId);

        verify(questionsService, times(1)).delete(fakeId);
    }




}
