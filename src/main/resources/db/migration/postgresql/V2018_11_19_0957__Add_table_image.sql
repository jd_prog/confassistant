CREATE TABLE attachment
(
  id bigint PRIMARY KEY NOT NULL ,
  name varchar(100) NOT NULL,
  type varchar(100) NOT NULL,
  download_uri varchar(256),
  attachment bytea NOT NULL
);
CREATE UNIQUE INDEX attachment_id_uindex ON attachment (id);
