create table event_tags
(
  event_id bigint not null
    constraint fkhjeq0v70jnlh0glmfoatbqy0b
    references event,
  tags_id  bigint not null
    constraint fkbve6i3wh3ugwl1ni0k1a6s9bt
    references tag
);