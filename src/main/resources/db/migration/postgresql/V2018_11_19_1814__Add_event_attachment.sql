CREATE TABLE event_attachments
(
    event_id bigint,
    attachments_id bigint,
    CONSTRAINT event_attachments___fk FOREIGN KEY (event_id) REFERENCES event,
    CONSTRAINT event_attachments___fk_2 FOREIGN KEY (attachments_id) REFERENCES attachment
);