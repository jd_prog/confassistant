create table tag
(
  id   bigint not null
    constraint tag_pkey
    primary key,
  name varchar(255)
);

create table tag_relations
(
  tag_id           bigint not null
    constraint fko8tx1bnqlb5e1bwj2bl4oe5y1
    references tag,
  tag_relations_id bigint not null
    constraint fk77y8ll3qqisjvuy7i6u4wrpru references tag
);