ALTER TABLE tag_relations DROP CONSTRAINT fko8tx1bnqlb5e1bwj2bl4oe5y1;
ALTER TABLE tag_relations DROP CONSTRAINT fk77y8ll3qqisjvuy7i6u4wrpru;

ALTER TABLE tag_relations ADD CONSTRAINT tag_id_fk FOREIGN KEY (tag_id) REFERENCES tag(id);
ALTER TABLE tag_relations ADD CONSTRAINT tag_relations_id_fk FOREIGN KEY (tag_relations_id) REFERENCES tag(id);