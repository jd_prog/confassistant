package com.qq.confassistant.service;

import java.io.IOException;

public interface IQRService {
    byte [] getQRCodeImage(String text, int width, int height) throws Exception;
}
