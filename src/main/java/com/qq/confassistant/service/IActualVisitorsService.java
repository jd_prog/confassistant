package com.qq.confassistant.service;


import com.qq.confassistant.model.Event;
import com.qq.confassistant.model.PublicUser;

import java.util.List;

public interface IActualVisitorsService {


    PublicUser create(Long id, String username) ;

    List<PublicUser> getActualVisitorsByEvent(Long id);

    void removeEventActualVisitors(Event event);
}
