package com.qq.confassistant.service;

import com.qq.confassistant.model.Question;
import com.qq.confassistant.model.QuestionStatus;

/**
 * Created by yana on 12.04.18.
 */
public interface IQuestionsService {

    Iterable<Question> getAllByTopic(long topicId);

    Question get(long questionId);

    Question create(long topicId, String username, Question question);

    Question update(long topicId, Question question);

    Question vote(long questionId, String username);

    Question removeVote(long questionId, String username);

    void delete(long questionId);

    Question changeStatus(long questionId,
                          QuestionStatus status);


}
