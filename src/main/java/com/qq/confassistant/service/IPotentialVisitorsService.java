package com.qq.confassistant.service;


import com.qq.confassistant.model.Event;
import com.qq.confassistant.model.PublicUser;

import java.util.List;


public interface IPotentialVisitorsService {


    PublicUser create(Long eventId,String username) ;

    PublicUser  remove(Long eventId,String username);

    List<PublicUser> getAllPotentialVisitorsByEvent(Long eventId);

    void removeEventPotentialVisitors(Event event);
}
