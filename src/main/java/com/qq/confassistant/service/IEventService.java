package com.qq.confassistant.service;

import com.qq.confassistant.model.Event;
import com.qq.confassistant.model.PublicUser;
import com.qq.confassistant.model.Tag;

import java.util.Collection;
import java.util.List;
import java.util.Set;

/**
 * Created by yana on 12.04.18.
 */
public interface IEventService {

    Iterable<Event> getAll();

    Iterable<Event> getAllCreatedBy(String username);

    Iterable<Event> getPublished();

    Event get(long id);

    Event create(Event event);

    void delete(Long id);

    Event update(Event event);

    Event changeStatusToPublish(Long id);

    Set<Event> findByTagId(Long tagId);

    Set<Event> getAllByTags(Set<Tag> allRelatedTags);

    Set<Event> findByTextWithTags(String text);

    List<Event> findByTagNameIn(String name);

    List<Event> getAllByTag(Tag relTag);

    Event addAttachmentsToEvent(Long eventId, Long attachmentsId);

    Event deleteAttachment(Long eventId, Long attachmentsId);

    List<Event> saveGrabed(List<Event> grabed);
}

