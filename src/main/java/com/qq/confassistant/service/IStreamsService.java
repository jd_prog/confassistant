package com.qq.confassistant.service;

import com.qq.confassistant.model.Stream;

import java.util.List;

/**
 * Created by yana on 13.04.18.
 */
public interface IStreamsService {

    Iterable<Stream> getAll();

    Stream get(long id);

    Stream create(long eventId, Stream stream);

    void delete(Long id);

    Stream update(Stream stream);

    List<Stream> getStreamsByEvent(long eventId);

}
