package com.qq.confassistant.service;

import com.qq.confassistant.model.Tag;

import java.util.Collection;
import java.util.List;
import java.util.Set;

public interface ITagService {

    List<Tag> getAll();

    Tag getById(Long id);

    Tag create(Tag tag);

    Tag mapTagToAnotherTag(Long rootTagId, Long tagId);

    Set<Tag> findAllInTag(Tag tag);


    Tag addEvent(Long eventId, Long tagId);

    Set<Tag> findByNameIn(String text);

    void removeFromEvent(Long eventId, Long tagId);

    Tag findByOneTagNameIn(String name);

    Collection<String> reformatTextToList(String text);
}
