package com.qq.confassistant.service;

import com.qq.confassistant.model.Attachment;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

public interface IAttachmentService {
    Attachment create(MultipartFile attachment) throws IOException;
    byte[] findAttachmentById(Long id);
    void deleteAttachment( Long id);
    String get();
    }
