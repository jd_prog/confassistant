package com.qq.confassistant.service;

import com.qq.confassistant.model.PublicUser;
import com.qq.confassistant.model.SecurityUser;
import org.apache.catalina.User;

import java.util.List;
import java.util.Set;

/**
 * Created by yana on 01.05.18.
 */
public interface IUserService {

    Iterable<SecurityUser> getAll();

    SecurityUser save(SecurityUser user);

    SecurityUser saveAsAdmin(SecurityUser user);

    SecurityUser loadUserByUsername(String username);

    Iterable<PublicUser> getAllByEventId(long eventId);

    Iterable<PublicUser> getAllByTopicId(long topicId);

    Iterable<PublicUser> getAllPublicUsers();

    Set<PublicUser> allUsersByEvent(Long id);


}
