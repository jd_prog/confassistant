package com.qq.confassistant.service;



import com.qq.confassistant.model.Event;
import com.qq.confassistant.model.PublicUser;

import java.util.List;

public interface IEventSharedUsersService {

    List<PublicUser> shareUsers(Long eventId,List<PublicUser> list);

    void removeUsers(Long eventId, String username);

    List<PublicUser> getAllEventSharedUsers(Long id);

    List<Event> getSharedEvents(String username);

    void removeEventSharedUsers(Event event);
}
