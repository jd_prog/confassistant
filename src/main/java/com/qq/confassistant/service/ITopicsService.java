package com.qq.confassistant.service;

import com.qq.confassistant.model.Topic;

import java.util.Collection;

/**
 * Created by yana on 12.04.18.
 */
public interface ITopicsService {

    Collection<Topic> getAllByStreamId(long streamId);

    Topic get(long topicId);

    Topic create(long streamId, Topic topic);

    Topic update(long streamId, Topic topic);

    void delete(long topicId);
}


