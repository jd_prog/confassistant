package com.qq.confassistant.service.impl;

import com.qq.confassistant.dao.AttachmentDao;
import com.qq.confassistant.dao.EventsDao;
import com.qq.confassistant.model.Attachment;
import com.qq.confassistant.model.Event;
import com.qq.confassistant.service.IAttachmentService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.io.IOException;
import java.util.List;

@Service
public class AttachmentService implements IAttachmentService {
    public AttachmentDao attachmentsDao;
    public EventsDao eventDao;
    @Value(value = "${application.AdminServer}")
    String applicationServer;
    @Value(value = "${spring.http.multipart.max-file-size}")
    String maxFileSize;

    public AttachmentService(AttachmentDao attachmentsDao, EventsDao eventDao) {
        this.attachmentsDao = attachmentsDao;
        this.eventDao = eventDao;
    }

    @Override
    public Attachment create(MultipartFile attachmentFile) throws IOException {
        Attachment attachment = new Attachment();
        attachment.setAttachment(attachmentFile.getBytes());
        attachment.setName(attachmentFile.getOriginalFilename());
        attachment.setType(attachmentFile.getContentType());
        Attachment attachmentSaved = attachmentsDao.save(attachment);
        String downloadUri = ServletUriComponentsBuilder.fromUriString(applicationServer)
                .path("api/attachment/download/")
                .path(attachmentSaved.getId() + "/")
                .path(attachment.getName())
                .toUriString();
        attachment.setDownloadUri(downloadUri);
        attachmentSaved.setDownloadUri(downloadUri);
        return attachmentsDao.save(attachmentSaved);
    }

    @Override
    public byte[] findAttachmentById(Long id) {
        return attachmentsDao.findOne(id).getAttachment();
    }

    public void deleteAttachment(Long id) {
        attachmentsDao.delete(id);
    }
    public String get() {
        return maxFileSize;
    }

}
