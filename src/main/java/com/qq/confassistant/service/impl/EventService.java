package com.qq.confassistant.service.impl;

import com.qq.confassistant.dao.*;
import com.qq.confassistant.model.*;
import com.qq.confassistant.service.*;
import com.qq.confassistant.validation.scenario.EventValidationScenario;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;


import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.qq.confassistant.model.EventStatus.PUBLISH;

/**
 * Created by yana on 12.04.18.
 */
@Service
@AllArgsConstructor
public class EventService implements IEventService {

    private EventsDao eventsDao;
    private IActualVisitorsService actualVisitorsService;
    private IPotentialVisitorsService potentialVisitorsService;
    private IEventSharedUsersService eventSharedUsersService;
    private StreamsService streamsService;
    private EventValidationScenario validationScenario;
    private TagService tagService;
    private AttachmentDao attachmentsDao;
    private AttachmentService attachmentService;

    @Override
    public Iterable<Event> getAll() {
        return eventsDao.findAll();
    }

    @Override
    public Iterable<Event> getAllCreatedBy(String username) {
        return eventsDao.findAllByCreatorUsername(username);
    }

    @Override
    public Iterable<Event> getPublished() {
        return eventsDao.findEventsByStatus(PUBLISH);
    }

    @Override
    public Event get(long id) {
        validationScenario.checkIfExists(id);
        Event event = eventsDao.findOne(id);
        return event;
    }

    @Override
    public Event create(Event event) {

        validationScenario.checkIfCanCreate(event);

        return eventsDao.save(event);
    }

    @Override
    public void delete(Long id) {

        validationScenario.checkIfExists(id);
        Event event = eventsDao.findOne(id);
        eventSharedUsersService.removeEventSharedUsers(event);
        potentialVisitorsService.removeEventPotentialVisitors(event);
        actualVisitorsService.removeEventActualVisitors(event);
        eventsDao.delete(id);
    }

    @Override
    public Event update(Event event) {

        validationScenario.checkIfCanCreate(event);
        event.setStreams(streamsService.getStreamsByEvent(event.getId()));
        return eventsDao.save(event);
    }

    @Override
    public Event changeStatusToPublish(Long id) {
        eventsDao.findOne(id).setStatus(EventStatus.PUBLISH);
        return eventsDao.save(eventsDao.findOne(id));
    }

    @Override
    public Set<Event> findByTagId(Long tagId) {
        Tag rootTag = tagService.getById(tagId);
        Set<Tag> allTagsFromRoot = tagService.findAllInTag(rootTag);
        return getAllByTags(allTagsFromRoot);
    }

    @Override
    public Set<Event>  findByTextWithTags(String text) {
        return getAllByTags(tagService.findByNameIn(text));
    }

    @Override
    public Set<Event> getAllByTags(Set<Tag> allRelatedTags) {
        return allRelatedTags
                .stream()
                .flatMap(tag -> eventsDao.findAllByTags(tag).stream())
                .filter(event -> event.getStatus() == EventStatus.PUBLISH)
                .collect(Collectors.toSet());
    }

    @Override
    public List<Event> getAllByTag(Tag relTag) {
       return eventsDao.findAllByTags(relTag)
               .stream()
               .filter(event -> event.getStatus() == EventStatus.PUBLISH)
                .collect(Collectors.toList());
    }

    @Override
    public List<Event> saveGrabed(List<Event> grabed) {
        return this.eventsDao.save(grabed);
    }

    @Override
    public List<Event> findByTagNameIn(String name) {
        return getAllByTag(tagService.findByOneTagNameIn(name));
    }
    @Override
    public Event addAttachmentsToEvent(Long eventId, Long attachmentsId){
        Event event = eventsDao.findOne(eventId);
        List<Attachment> attachmentList = event.getAttachments();
        Attachment save = attachmentsDao.findOne(attachmentsId);
        attachmentList.add(save);
        event.setAttachments(attachmentList);
        return  eventsDao.save(event);
    }
    @Override
    public Event deleteAttachment(Long eventId, Long attachmentsId){
        Event event = eventsDao.findOne(eventId);
        Attachment attachment = attachmentsDao.findOne(attachmentsId);
        event.getAttachments().remove(attachment);
        eventsDao.save(event);
        attachmentService.deleteAttachment(attachmentsId);
        return event;
    }

}
