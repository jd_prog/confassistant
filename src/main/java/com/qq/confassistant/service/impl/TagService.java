package com.qq.confassistant.service.impl;

import com.qq.confassistant.dao.EventsDao;
import com.qq.confassistant.dao.TagDao;
import com.qq.confassistant.model.Event;
import com.qq.confassistant.model.Tag;
import com.qq.confassistant.service.ITagService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class TagService implements ITagService {

    final TagDao tagDao;
    final EventsDao eventsDao;

    @Override
    public List<Tag> getAll() {
        return tagDao.findAll();
    }

    @Override
    public Tag getById(Long id) {
        return tagDao.findOne(id);
    }

    @Override
    public Tag create(Tag tag) {
      return tagDao.findAll()
                .stream()
                .filter(tag1 -> tag1.getName().equals(tag.getName()))
                .findFirst()
                .orElseGet(() -> tagDao.save(tag));
    }

    @Override
    public Tag mapTagToAnotherTag(Long rootTagId, Long tagId) {
        Tag rootTag = tagDao.findOne(rootTagId);
        Tag tagToMap = tagDao.findOne(tagId);
        rootTag.getTagRelations().add(tagToMap);
        tagDao.save(rootTag);
        return tagToMap;
    }

    @Override
    public Set<Tag> findAllInTag(Tag tag) {
        Set<Tag> tags = new HashSet<>();
        tags.add(tag);
        tags.addAll(tag.getTagRelations().stream().flatMap(tag1 -> findAllInTag(tag1).stream()).collect(Collectors.toSet()));
        return  tags;
    }

    @Override
    public Tag addEvent(Long eventId, Long tagId) {
        Event event = eventsDao.findOne(eventId);
        Tag tagToAdd = tagDao.findOne(tagId);
        List<Tag> tags = event.getTags();
        Tag savedTag = tagDao.save(tagToAdd);
        tags.add(savedTag);
        eventsDao.save(event);
        return savedTag;
    }

    @Override
    public Set<Tag> findByNameIn(String text) {
        return tagDao.findByNameIn(reformatTextToList(text))
                .stream()
                .flatMap(tag -> findAllInTag(tag).stream())
                .collect(Collectors.toSet());
    }
    @Override
    public Collection<String> reformatTextToList(String text) {
        return Arrays.asList(text.split(" "));
    }
    @Override
    public Tag findByOneTagNameIn(String name) {
        Tag tag = tagDao.getTagByName(name);
        return tag;
    }

    @Override
    public void removeFromEvent(Long eventId, Long tagId) {
        Event event = this.eventsDao.findOne(eventId);
        Tag tagToRemove = this.tagDao.findOne(tagId);
        List<Tag> eventTags = event.getTags();
        eventTags.remove(tagToRemove);
        eventsDao.save(event);
    }

}
