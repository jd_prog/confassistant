package com.qq.confassistant.service.impl;

import com.qq.confassistant.dao.EventActualVisitorsDao;
import com.qq.confassistant.dao.EventsDao;
import com.qq.confassistant.dao.PublicUserDao;
import com.qq.confassistant.model.Event;
import com.qq.confassistant.model.EventActualVisitors;
import com.qq.confassistant.model.PublicUser;
import com.qq.confassistant.service.IActualVisitorsService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class ActualVisitorsService implements IActualVisitorsService {

    private EventActualVisitorsDao eventActualVisitorsDao;
    private PublicUserDao publicUserDao;
    private EventsDao eventsDao;



    public PublicUser create(Long id, String username) {
        Event event = eventsDao.findOne(id);
        PublicUser publicUser = publicUserDao.findOne(username);
        return eventActualVisitorsDao.save(new EventActualVisitors(event, publicUser)).getUser();
    }

    public List<PublicUser> getActualVisitorsByEvent(Long id){
        Event event = eventsDao.findOne(id);
        return eventActualVisitorsDao.findAllByEvent(event)
                .stream()
                .map(EventActualVisitors::getUser)
                .collect(Collectors.toList());
    }

    @Override
    public void removeEventActualVisitors(Event event) {
        eventActualVisitorsDao.deleteEventActualVisitorsByEvent_Id(event.getId());
    }

}
