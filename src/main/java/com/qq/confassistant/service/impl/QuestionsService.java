package com.qq.confassistant.service.impl;

import com.qq.confassistant.dao.PublicUserDao;
import com.qq.confassistant.dao.QuestionsDao;
import com.qq.confassistant.dao.TopicsDao;
import com.qq.confassistant.dao.VotesDao;
import com.qq.confassistant.model.PublicUser;
import com.qq.confassistant.model.Question;
import com.qq.confassistant.model.QuestionStatus;
import com.qq.confassistant.model.Vote;
import com.qq.confassistant.service.IQuestionsService;
import com.qq.confassistant.validation.scenario.QuestionValidationScenario;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

/**
 * Created by yana on 12.04.18.
 */
@Service
@AllArgsConstructor
public class QuestionsService implements IQuestionsService {

    private QuestionsDao questionsDao;
    private TopicsDao topicsDao;
    private VotesDao votesDao;
    private PublicUserDao userService;

    private QuestionValidationScenario validationScenario;

    @Override
    public Iterable<Question> getAllByTopic(long topicId) {
        return questionsDao.findAllByTopic(topicId);
    }

    @Override
    public Question get(long questionId) {

        validationScenario.checkIfExists(questionId);

        return questionsDao.findOne(questionId);
    }

    @Override
    public Question create(long topicId, String username, Question question) {

        validationScenario.checkIfCanBeCreated(topicId);

        PublicUser user = userService.findOne(username);
        question.setAuthor(user);
        question.setAuthorDisplayName(user.getDisplayName());
        question.setAuthorUserName(username);
        question.setTopic(topicsDao.findOne(topicId));

        return questionsDao.save(question);
    }

    @Override
    public Question update(long topicId, Question question) {

        validationScenario.checkIfCanBeUpdated(topicId, question.getId());

        Question oldQuestion = questionsDao.findOne(question.getId());
        question.setAuthor(oldQuestion.getAuthor());
        question.setAuthorDisplayName(oldQuestion.getAuthorDisplayName());
        question.setAuthorUserName(oldQuestion.getAuthorUserName());
        question.setTopic(oldQuestion.getTopic());

        return questionsDao.save(question);
    }

    @Override
    public Question vote(long questionId, String username) {

        validationScenario.checkIfCanBeVoted(questionId, username);

        Question question = get(questionId);
        Vote vote = new Vote();
        vote.setUsername(username);
        vote.setQuestion(question);

        votesDao.save(vote);

        return questionsDao.findOne(questionId);
    }

    @Override
    @Transactional
    public Question removeVote(long questionId, String username) {

        validationScenario.checkIfExists(questionId);

        votesDao.deleteAllByQuestionAndUsername(get(questionId), username);

        return get(questionId);
    }

    @Override
    public void delete(long questionId) {

        validationScenario.checkIfExists(questionId);

        questionsDao.delete(questionId);
    }

    @Override
    public Question changeStatus(long questionId, QuestionStatus status) {

        validationScenario.checkIfExists(questionId);

        if (status == QuestionStatus.OPENED) closeOpenedQuestions();

        return applyStatus(questionsDao.findOne(questionId), status);
    }

    private void closeOpenedQuestions() {
        questionsDao.findAllByQuestionStatus(QuestionStatus.OPENED)
                .forEach(question -> applyStatus(question, QuestionStatus.CLOSED));
    }

    private Question applyStatus(Question question, QuestionStatus status) {
        question.setQuestionStatus(status);

        return questionsDao.save(question);
    }
}