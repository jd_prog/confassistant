package com.qq.confassistant.service.impl;

import com.qq.confassistant.dao.EventSharedUsersDao;
import com.qq.confassistant.dao.EventsDao;
import com.qq.confassistant.dao.PublicUserDao;
import com.qq.confassistant.model.Event;
import com.qq.confassistant.model.EventSharedUsers;
import com.qq.confassistant.model.PublicUser;
import com.qq.confassistant.service.IEventSharedUsersService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class EventSharedUsersService implements IEventSharedUsersService {
    private EventSharedUsersDao eventSharedUsersDao;
    private EventsDao eventsDao;
    private PublicUserDao publicUserDao;

    @Override
    public List<PublicUser> shareUsers(Long eventId, List<PublicUser> list) {
        eventSharedUsersDao.deleteEventSharedUsersByEvent_Id(eventId);
        Event event = eventsDao.findOne(eventId);
        return eventSharedUsersDao.save(list
                .stream()
                .map(p -> new EventSharedUsers(event, p))
                .collect(Collectors.toList()))
                .stream()
                .map(epv -> epv.getUser())
                .collect(Collectors.toList());
    }

    @Override
    public void removeUsers(Long eventId, String username) {
        Event event = eventsDao.findOne(eventId);
        PublicUser publicUser = publicUserDao.findOne(username);
        eventSharedUsersDao.delete(new EventSharedUsers(event, publicUser));
    }

    @Override
    public List<PublicUser> getAllEventSharedUsers(Long id) {
        Event event = eventsDao.findOne(id);
        return eventSharedUsersDao.findAllByEvent(event)
                .stream()
                .map(EventSharedUsers::getUser)
                .collect(Collectors.toList());
    }

    @Override
    public List<Event> getSharedEvents(String username) {
        PublicUser publicUser = publicUserDao.findOne(username);
        return eventSharedUsersDao.findAllByUser(publicUser)
                .stream()
                .map(EventSharedUsers::getEvent)
                .collect(Collectors.toList());
    }

    @Override
    public void removeEventSharedUsers(Event event) {
        eventSharedUsersDao.deleteEventSharedUsersByEvent_Id(event.getId());
    }
}
