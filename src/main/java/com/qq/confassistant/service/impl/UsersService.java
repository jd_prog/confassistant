package com.qq.confassistant.service.impl;


import com.qq.confassistant.dao.EventsDao;
import com.qq.confassistant.dao.PublicUserDao;
import com.qq.confassistant.dao.TopicsDao;
import com.qq.confassistant.model.*;
import com.qq.confassistant.dao.SecurityUserDao;
import com.qq.confassistant.service.IActualVisitorsService;
import com.qq.confassistant.service.IPotentialVisitorsService;
import com.qq.confassistant.service.IUserService;
import lombok.AllArgsConstructor;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;

/**
 * Created by yana on 01.05.18.
 */

@Service @AllArgsConstructor
public class UsersService implements UserDetailsService, IUserService {
    private SecurityUserDao userDao;
    private PublicUserDao publicUserDao;
    private BCryptPasswordEncoder passwordEncoder;
    private IActualVisitorsService actualVisitorsService;
    private IPotentialVisitorsService potentialVisitorsService;

    @Override
    public SecurityUser loadUserByUsername(String username) throws UsernameNotFoundException {
        return userDao.findOne(username);
    }

    @Override
    public Iterable<SecurityUser> getAll() {
        return userDao.findAll();
    }

    @Override
    public Iterable<PublicUser> getAllPublicUsers() {
        return publicUserDao.findAll();
    }

    @Override
    public Set<PublicUser> allUsersByEvent(Long id) {
        Set<PublicUser> eventActualVisitorsSet = new HashSet<>(actualVisitorsService.getActualVisitorsByEvent(id));
        Set<PublicUser> eventPotentialVisitorsSet = new HashSet<>(potentialVisitorsService.getAllPotentialVisitorsByEvent(id));
        eventActualVisitorsSet.addAll(eventPotentialVisitorsSet);
        return eventActualVisitorsSet;
    }

    @Override
    public SecurityUser save(SecurityUser user){

        SecurityUser existingSecureUser = loadUserByUsername(user.getUsername());

        if (existingSecureUser != null){
            user.setRole(existingSecureUser.getRole());
        }

        String password = user.getPassword() == null
                ? randomAlphabetic(8)
                : user.getPassword();

        user.setPassword(passwordEncoder.encode(password));

        SecurityUser saved = userDao.save(user);
        saved.setPassword(password);  

        return saved;
    }


    @Override
    public SecurityUser saveAsAdmin(SecurityUser user) {

        String password = user.getPassword() == null
                ? randomAlphabetic(8)
                : user.getPassword();

        user.setRole(Role.ROLE_ADMIN);
        user.setPassword(passwordEncoder.encode(password));

        SecurityUser saved = userDao.save(user);
        saved.setPassword(password);

        return saved;
    }
    @Override
    public Iterable<PublicUser> getAllByEventId(long eventId) {


        return publicUserDao.findAllByEvent(eventId);
    }

    public Iterable<PublicUser> getAllByTopicId(long topicId) {


        return publicUserDao.getAllByTopicId(topicId);
    }
}

