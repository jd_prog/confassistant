package com.qq.confassistant.service.impl;

import com.qq.confassistant.dao.StreamsDao;
import com.qq.confassistant.dao.TopicsDao;
import com.qq.confassistant.model.Stream;
import com.qq.confassistant.model.Topic;
import com.qq.confassistant.service.ITopicsService;
import com.qq.confassistant.validation.scenario.TopicValidationScenario;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Collection;

/**
 * Created by yana on 12.04.18.
 */
@Service
@AllArgsConstructor
public class TopicsService implements ITopicsService {

    private TopicsDao topicsDao;
    private StreamsDao streamsDao;
    private TopicValidationScenario validationScenario;

    @Override
    public Collection<Topic> getAllByStreamId(long streamId) {

        validationScenario.checkIfStreamExists(streamId);

        return topicsDao.findTopicsByStream(streamsDao.findOne(streamId));
    }

    @Override
    public Topic get(long topicId) {

        validationScenario.checkIfTopicExists(topicId);

        return topicsDao.findOne(topicId);
    }

    @Override
    public Topic create(long streamId, Topic topic) {

        validationScenario.checkIfStreamExists(streamId);

        return save(streamId, topic);
    }

    @Override
    public Topic update(long streamId, Topic topic) {

        validationScenario.checkIfCanBeUpdated(streamId, topic.getId());

        return save(streamId, topic);
    }

    @Override
    public void delete(long topicId) {

        validationScenario.checkIfTopicExists(topicId);

        topicsDao.delete(topicId);
    }


    private Topic save(long streamId, Topic topic) {

        Stream stream = streamsDao.findOne(streamId);
        topic.setStream(stream);

        return topicsDao.save(topic);
    }
}
