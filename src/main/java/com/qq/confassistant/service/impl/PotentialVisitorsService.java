package com.qq.confassistant.service.impl;

import com.qq.confassistant.dao.EventPotentialVisitorsDao;
import com.qq.confassistant.dao.EventsDao;
import com.qq.confassistant.dao.PublicUserDao;
import com.qq.confassistant.model.Event;
import com.qq.confassistant.model.EventPotentialVisitors;
import com.qq.confassistant.model.PublicUser;
import com.qq.confassistant.service.IPotentialVisitorsService;
import com.qq.confassistant.validation.scenario.EventValidationScenario;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class PotentialVisitorsService implements IPotentialVisitorsService {

    private EventsDao eventsDao;
    private PublicUserDao publicUserDao;
    private EventPotentialVisitorsDao eventPotentialVisitorsDao;
    private EventValidationScenario validationScenario;


    @Override
    public PublicUser create(Long eventId,String username) {
        validationScenario.checkIfCanJoinEvent(eventId, username);
        Event event = eventsDao.findOne(eventId);
        PublicUser publicUser = publicUserDao.findOne(username);
        return  eventPotentialVisitorsDao.save(new EventPotentialVisitors(event, publicUser)).getUser();

    }

    @Override
    public PublicUser remove(Long eventId,String username) {
        validationScenario.checkIfCanLeftEvent(eventId, username);
        Event event = eventsDao.findOne(eventId);
        PublicUser publicUser = publicUserDao.findOne(username);
        eventPotentialVisitorsDao.delete(new EventPotentialVisitors(event,publicUser));
        return publicUser;
    }


    @Override
    public List<PublicUser> getAllPotentialVisitorsByEvent(Long eventId) {
        Event event = eventsDao.findOne(eventId);
        return eventPotentialVisitorsDao.findAllByEvent(event)
                .stream()
                .map(EventPotentialVisitors::getUser)
                .collect(Collectors.toList());
    }

    @Override
    public void removeEventPotentialVisitors(Event event){
        eventPotentialVisitorsDao.deleteEventPotentialVisitorsByEvent_Id(event.getId());
    }
}
