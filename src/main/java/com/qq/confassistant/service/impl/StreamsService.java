package com.qq.confassistant.service.impl;

import com.qq.confassistant.dao.EventsDao;
import com.qq.confassistant.dao.StreamsDao;
import com.qq.confassistant.model.Stream;
import com.qq.confassistant.service.IStreamsService;
import com.qq.confassistant.validation.scenario.StreamValidationScenario;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by yana on 13.04.18.
 */
@Service @AllArgsConstructor
public class StreamsService implements IStreamsService {

    private StreamsDao streamsDao;
    private EventsDao eventsDao;
    private StreamValidationScenario validationScenario;

    @Override
    public Iterable<Stream> getAll() {
        return streamsDao.findAll();
    }

    @Override
    public Stream get(long id) {

        validationScenario.checkIfStreamExists(id);

        return streamsDao.findOne(id);
    }

    @Override
    public Stream create(long eventId, Stream stream) {

        validationScenario.checkIfEventExists(eventId);

        stream.setEvent(eventsDao.findOne(eventId));
        return streamsDao.save(stream);
    }


    @Override
    public void delete(Long id) {

        validationScenario.checkIfStreamExists(id);

        streamsDao.delete(id);
    }

    @Override
    public Stream update(Stream stream) {

        validationScenario.checkIfStreamExists(stream.getId());

        return streamsDao.save(stream);
    }

    @Override
    public List<Stream> getStreamsByEvent(long eventId){

        validationScenario.checkIfEventExists(eventId);

        return streamsDao.findStreamsByEvent(eventsDao.findOne(eventId));
    }
}
