package com.qq.confassistant.validation.scenario;

import com.qq.confassistant.model.Stream;
import com.qq.confassistant.model.Topic;
import com.qq.confassistant.validation.validator.EntityExistsValidator;
import com.qq.confassistant.validation.ValidationExplorer;
import com.qq.confassistant.validation.utils.ValidatorUtils;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class TopicValidationScenario {

    private EntityExistsValidator<Stream, Long> streamValidator;
    private EntityExistsValidator<Topic, Long> topicValidator;
    private ValidationExplorer validationExplorer;

    public void checkIfStreamExists(long streamId) {

        validationExplorer
                .when(streamValidator.notExists(streamId))
                .thenThrow(ValidatorUtils.notFound(Stream.class, String.valueOf(streamId)));
    }

    public void checkIfTopicExists(long topicId) {

        validationExplorer
                .when(topicValidator.notExists(topicId))
                .thenThrow(ValidatorUtils.notFound(Topic.class, String.valueOf(topicId)));
    }

    public void checkIfCanBeUpdated(long streamId, long topicId) {

        validationExplorer
                .when(streamValidator.notExists(streamId))
                .thenThrow(ValidatorUtils.notFound(Stream.class, String.valueOf(streamId)))
                .when(topicValidator.notExists(topicId))
                .thenThrow(ValidatorUtils.notFound(Topic.class, String.valueOf(topicId)));
    }
}
