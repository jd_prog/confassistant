package com.qq.confassistant.validation.scenario;

import com.qq.confassistant.model.Question;
import com.qq.confassistant.model.Topic;
import com.qq.confassistant.validation.validator.QuestionValidator;
import com.qq.confassistant.validation.ValidationExplorer;
import com.qq.confassistant.validation.utils.ValidatorUtils;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class QuestionValidationScenario {

    private QuestionValidator questionValidator;
    private ValidationExplorer validationExplorer;

    public void checkIfCanBeVoted(long questionId, String username) {
        validationExplorer
                .when(questionValidator.notExists(questionId))
                .thenThrow(ValidatorUtils.notFound(Question.class, String.valueOf(questionId)))
                .when(questionValidator.isCreatedByUser(questionId, username))
                .thenThrow(ValidatorUtils.actionNotAllowed("Vote", "Question can't be voted by its author"))
                .when(questionValidator.isVotedByUser(questionId, username))
                .thenThrow(ValidatorUtils.actionNotAllowed("Vote", String.format("Question is already voted by %s", username)));
    }

    public void checkIfCanBeUpdated(long topicId, long questionId) {

        validationExplorer
                .when(questionValidator.topicNotExists(topicId))
                .thenThrow(ValidatorUtils.notFound(Topic.class, String.valueOf(topicId)))
                .when(questionValidator.notExists(questionId))
                .thenThrow(ValidatorUtils.notFound(Question.class, String.valueOf(questionId)))
                .when(questionValidator.hasVotes(questionId))
                .thenThrow(ValidatorUtils.actionNotAllowed("Update", "Question voted by users can't be updated"));
    }

    public void checkIfCanBeCreated(long topicId) {
        validationExplorer
                .when(questionValidator.topicNotExists(topicId))
                .thenThrow(ValidatorUtils.notFound(Topic.class, String.valueOf(topicId)));
    }

    public void checkIfExists(long questionId) {
        validationExplorer
                .when(questionValidator.notExists(questionId))
                .thenThrow(ValidatorUtils.notFound(Question.class, String.valueOf(questionId)));
    }

}
