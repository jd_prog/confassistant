package com.qq.confassistant.validation.scenario;

import com.qq.confassistant.model.Event;
import com.qq.confassistant.validation.ValidationExplorer;
import com.qq.confassistant.validation.utils.ValidatorUtils;
import com.qq.confassistant.validation.validator.EventValidator;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class EventValidationScenario {

    private EventValidator eventValidator;
    private ValidationExplorer validationExplorer;

    public void checkIfExists(long eventId) {

        validationExplorer
                .when(eventValidator.notExists(eventId))
                .thenThrow(ValidatorUtils.notFound(Event.class, String.valueOf(eventId)));
    }

    public void checkIfCanCreate(Event event) {

        validationExplorer
                .when(eventValidator.creatorNotRegistered(event.getCreator()))
                .thenThrow(ValidatorUtils.badRequest("Event creator doesn't registered in the system"));
    }


    public void checkIfCanJoinEvent(long eventId, String username) {

        validationExplorer
                .when(eventValidator.notExists(eventId))
                .thenThrow(ValidatorUtils.notFound(Event.class, String.valueOf(eventId)));

    }

    public void checkIfCanLeftEvent(long eventId, String username) {

        validationExplorer
                .when(eventValidator.notExists(eventId))
                .thenThrow(ValidatorUtils.notFound(Event.class, String.valueOf(eventId)));
    }

}
