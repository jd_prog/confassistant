package com.qq.confassistant.validation.scenario;

import com.qq.confassistant.model.Event;
import com.qq.confassistant.model.Stream;
import com.qq.confassistant.validation.validator.EntityExistsValidator;
import com.qq.confassistant.validation.ValidationExplorer;
import com.qq.confassistant.validation.utils.ValidatorUtils;
import com.qq.confassistant.validation.validator.EventValidator;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class StreamValidationScenario {

    private EntityExistsValidator<Stream, Long> streamValidator;
    private EventValidator eventValidator;
    private ValidationExplorer validationExplorer;

    public void checkIfStreamExists(long streamId) {

        validationExplorer
                .when(streamValidator.notExists(streamId))
                .thenThrow(ValidatorUtils.notFound(Stream.class, String.valueOf(streamId)));
    }

    public void checkIfEventExists(long eventId) {

        validationExplorer
                .when(eventValidator.notExists(eventId))
                .thenThrow(ValidatorUtils.notFound(Event.class, String.valueOf(eventId)));
    }
}
