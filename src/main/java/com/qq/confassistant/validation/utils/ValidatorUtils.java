package com.qq.confassistant.validation.utils;

import com.qq.confassistant.exception.ActionNotAllowedException;
import com.qq.confassistant.exception.EntityNotFoundException;
import com.qq.confassistant.exception.ForbiddenException;

public class ValidatorUtils {

    public static <ID> EntityNotFoundException notFound(Class clazz, ID id){

        return new EntityNotFoundException(clazz, id.toString());
    }

    public static ActionNotAllowedException actionNotAllowed(String action, String reason){
        return new ActionNotAllowedException(action,reason);
    }
    public static ForbiddenException actionForbidden( String reason){
        return new ForbiddenException(reason);
    }

    public static IllegalArgumentException badRequest(String message){
        return new IllegalArgumentException(message);
    }
}
