package com.qq.confassistant.validation;

import org.springframework.stereotype.Component;

@Component
public class ValidationExplorer {

    private boolean condition = false;

    public ValidationExplorer when(boolean condition) {
        this.condition = condition;

        return this;
    }

    public ValidationExplorer thenThrow(RuntimeException exception) {

        if (!condition){
            return this;
        }

        throw exception;
    }
}
