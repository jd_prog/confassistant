package com.qq.confassistant.validation.validator;

import com.qq.confassistant.dao.EventsDao;
import com.qq.confassistant.dao.PublicUserDao;
import com.qq.confassistant.model.Event;
import com.qq.confassistant.model.EventStatus;
import com.qq.confassistant.model.PublicUser;
import lombok.AllArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@AllArgsConstructor
public class EventValidator {

    private EventsDao eventsDao;
    private PublicUserDao userDao;

    public boolean notExists(long eventId){
        return !eventsDao.exists(eventId);
    }


    public boolean creatorNotRegistered(PublicUser creator) {
        return !userDao.exists(creator.getUsername());
    }


    private boolean currentUserIsCreatorOfEvent(Event event) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return event.getCreator().getUsername().equals(authentication.getName());
    }

}
