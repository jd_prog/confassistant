package com.qq.confassistant.validation.validator;

import com.qq.confassistant.dao.QuestionsDao;
import com.qq.confassistant.dao.TopicsDao;
import com.qq.confassistant.dao.VotesDao;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class QuestionValidator{

    private final QuestionsDao questionsDao;
    private final VotesDao votesDao;
    private final TopicsDao topicsDao;

    public boolean notExists(long id) {
        return !questionsDao.exists(id);
    }

    public boolean topicNotExists(long id) {
        return !topicsDao.exists(id);
    }

    public boolean isCreatedByUser(long questionId, String username) {

        return questionsDao.countAllByIdAndAuthorUsername(questionId, username) > 0;
    }

    public boolean isVotedByUser(long questionId, String username) {
        return votesDao.countAllByQuestionIdAndUsername(questionId, username) > 0;
    }

    public boolean hasVotes(long questionId) {
        return votesDao.countByQuestionId(questionId) > 0;
    }
}
