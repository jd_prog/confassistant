package com.qq.confassistant.validation.validator;

import lombok.AllArgsConstructor;
import org.springframework.data.repository.CrudRepository;

import java.io.Serializable;

@AllArgsConstructor
public class EntityExistsValidator<T, ID extends Serializable> {

    private CrudRepository<T, ID> repository;

    public boolean notExists(ID id) {
        return !repository.exists(id);
    }
}
