package com.qq.confassistant.handler;

import com.qq.confassistant.exception.ActionNotAllowedException;
import com.qq.confassistant.exception.ForbiddenException;
import com.qq.confassistant.exception.EntityNotFoundException;
import com.qq.confassistant.model.ErrorInfo;
import org.springframework.beans.TypeMismatchException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.ArrayList;
import java.util.List;

@ControllerAdvice
@RestController
public class CustomRestExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(ActionNotAllowedException.class)
    public ResponseEntity handleIllegalActionException(ActionNotAllowedException exception, WebRequest request) {

        return generateResponse(
                HttpStatus.BAD_REQUEST, exception,
                String.format("Action '%s' can't be performed on this entity by '%s'",
                        exception.getAction(), request.getUserPrincipal().getName()));
    }
    @ExceptionHandler(ForbiddenException.class)
    public ResponseEntity handleIllegalActionException(ForbiddenException exception, WebRequest request) {

        return generateResponse(
                HttpStatus.FORBIDDEN, exception,
                String.format("Action '%s' can't be performed on this entity by '%s'",
                        request.getUserPrincipal().getName()));
    }

    @ExceptionHandler(EntityNotFoundException.class)
    public ResponseEntity handleEntityNotFoundException(EntityNotFoundException exception) {

        return generateResponse(
                HttpStatus.NOT_FOUND, exception,
                String.format("Entity of type %s with id %s doesn't exists'",
                        exception.getType().getSimpleName(), exception.getEntityId()));
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException exception,
                                                          HttpHeaders headers,
                                                          HttpStatus status,
                                                          WebRequest request) {

        return generateResponse(HttpStatus.BAD_REQUEST, exception, parseErrorMessages(exception.getBindingResult()));

    }

    @Override
    protected ResponseEntity<Object> handleBindException(BindException exception,
                                                 HttpHeaders headers,
                                                 HttpStatus status,
                                                 WebRequest request) {

        return generateResponse(HttpStatus.BAD_REQUEST, exception, parseErrorMessages(exception.getBindingResult()));
    }

    @Override
    protected ResponseEntity<Object> handleTypeMismatch(TypeMismatchException exception,
                                                HttpHeaders headers,
                                                HttpStatus status,
                                                WebRequest request) {
        return generateResponse(
                HttpStatus.NOT_FOUND, exception,
                String.format("%s value for %s should be of type %s",
                        exception.getValue(), exception.getPropertyName(), exception.getRequiredType()));
    }

    @Override
    protected ResponseEntity<Object> handleNoHandlerFoundException(NoHandlerFoundException exception,
                                                           HttpHeaders headers,
                                                           HttpStatus status,
                                                           WebRequest request) {
        return generateResponse(
                HttpStatus.NOT_FOUND, exception,
                String.format("No handler found for %s %s", exception.getHttpMethod(), exception.getRequestURL()));
    }

    @Override
    protected ResponseEntity<Object> handleHttpRequestMethodNotSupported(HttpRequestMethodNotSupportedException exception,
                                                                 HttpHeaders headers,
                                                                 HttpStatus status,
                                                                 WebRequest request) {
        return generateResponse(
                HttpStatus.METHOD_NOT_ALLOWED, exception,
                String.format("%s method is not supported for this request.", exception.getMethod()));
    }

    private String[] parseErrorMessages(BindingResult bindingResult) {
        List<String> errors = new ArrayList<>();

        bindingResult.getFieldErrors().forEach(error -> errors.add(error.getField() + ": " + error.getDefaultMessage()));
        bindingResult.getGlobalErrors().forEach(error -> errors.add(error.getObjectName() + ": " + error.getDefaultMessage()));

        return errors.toArray(new String[errors.size()]);
    }

    private ResponseEntity<Object> generateResponse(HttpStatus httpStatus, Exception exception, String... errorMessages) {

        return new ResponseEntity<>(
                new ErrorInfo(httpStatus, exception.getClass().getName(), exception.getLocalizedMessage(), errorMessages),
                httpStatus);
    }
}