package com.qq.confassistant.id;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@Embeddable
public class EventPublicUserId implements Serializable {

    @Column(name = "event_id")
    Long eventId;

    String username;




}
