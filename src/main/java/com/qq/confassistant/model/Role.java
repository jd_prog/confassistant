package com.qq.confassistant.model;

/**
 * Created by yana on 01.05.18.
 */
public enum Role {

    ROLE_USER, ROLE_ADMIN, ROLE_BASIC
}
