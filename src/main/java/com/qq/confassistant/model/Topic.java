package com.qq.confassistant.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by yana on 12.04.18.
 */
@Entity
@Getter
@Setter
@NoArgsConstructor
public class Topic {

    @Id
    @GeneratedValue
    private long id;

    @NotNull(message = "{validation.topic.title.notNull")
    @NotEmpty(message = "{validation.topic.title.notEmpty")
    @Size(min=1, max=255, message = "{validation.topic.title.size")
    private String title;



    @Column(length = 2000)
    private String description;

    @Temporal(TemporalType.TIMESTAMP)
    private Date startDate;

    @Temporal(TemporalType.TIMESTAMP)
    private Date endDate;

    @JsonIgnoreProperties(value={"topics"})
    @OneToMany(mappedBy = "topic", orphanRemoval = true, fetch = FetchType.LAZY)
    private List<Question> questions = new ArrayList<>();

    @ManyToMany
    private List<PublicUser> users = new ArrayList<>();

    @JsonIgnore
    @ManyToOne
    private Stream stream;

}
