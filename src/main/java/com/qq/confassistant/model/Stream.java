package com.qq.confassistant.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by yana on 13.04.18.
 */
@Entity
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value={"topics"})
public class Stream {

    @Id
    @GeneratedValue
    private long id;

    @NotNull(message = "{validation.stream.name.notNull}")
    @NotEmpty(message = "{validation.stream.name.notEmpty}")
    private String name;

    private String location;

    @OneToMany(mappedBy = "stream",
            cascade = CascadeType.ALL, orphanRemoval = true,
            fetch = FetchType.LAZY)
    private List<Topic> topics = new ArrayList<>();

    @JsonIgnore
    @ManyToOne
    private Event event;
}
