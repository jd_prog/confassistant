package com.qq.confassistant.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.*;

/**
 * Created by yana on 12.04.18.
 */
@Entity
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value={"streams"})
public class Event implements Serializable {

    @Id
    @GeneratedValue
    private long id;

    @NotNull(message = "{validation.event.title.notNull}")
    @NotEmpty(message = "{validation.event.title.notEmpty}")
    @Size(min = 1, max = 255, message = "{validation.event.title.size}")
    private String title;

    private String description;

    @Temporal(TemporalType.TIMESTAMP)
    @NotNull(message = "{validation.event.dateStart.notNull}")
    private Date dateStart;

    @NotNull(message = "{validation.event.dateEnd.notNull}")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateEnd;

    @Enumerated(EnumType.STRING)
    private EventStatus status = EventStatus.NOT_PUBLISH;

    @OneToMany(mappedBy = "event",
            fetch = FetchType.LAZY,
            cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Stream> streams = new ArrayList<>();

    @OneToMany(orphanRemoval = true)
    private List<Attachment> attachments;

    @OneToOne(cascade = CascadeType.ALL)
    private Location location = new Location();

    @OneToOne
    @NotNull
    private PublicUser creator;

    @ManyToMany
    private List<Tag> tags;


    private Integer participantsLimit;
}