package com.qq.confassistant.model;

/**
 * Created by yana on 12.04.18.
 */
public enum QuestionStatus {

    NEW, OPENED, CLOSED
}
