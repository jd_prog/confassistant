package com.qq.confassistant.model;

public enum EventStatus {
    PUBLISH, NOT_PUBLISH
}
