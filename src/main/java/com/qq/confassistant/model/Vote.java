package com.qq.confassistant.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import java.util.Objects;

@Entity
@Getter @Setter @NoArgsConstructor
public class Vote {

    @Id
    @GeneratedValue
    private long id;

    private String username;

    @JsonIgnore
    @ManyToOne
    private Question question;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Vote vote = (Vote) o;
        return Objects.equals(username, vote.username);
    }

    @Override
    public int hashCode() {

        return Objects.hash(username);
    }
}
