package com.qq.confassistant.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * Created by yana on 16.04.18.
 */
@Entity
@Getter @Setter @NoArgsConstructor
public class Location {

    @Id
    @GeneratedValue
    private long id;

    private double latitude;
    private double longitude;

    private String formattedAddress;

    private String cityName;

    private String placeId;
}
