package com.qq.confassistant.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * Created by yana on 12.04.18.
 */
@Entity
@Getter @Setter @NoArgsConstructor
public class Question {

    @Id
    @GeneratedValue
    private long id;

    @Column(length = 1000)
    @NotNull(message = "{validation.question.content.notNull}")
    @NotEmpty(message = "{validation.question.content.notEmpty}")
    @Size(min = 1, max = 1000, message = "{validation.question.content.size}")
    private String content;

    @OneToMany(mappedBy = "question", orphanRemoval = true)
    private List<Vote> votes = new ArrayList<>();

    @Enumerated(EnumType.STRING)
    private QuestionStatus questionStatus = QuestionStatus.NEW;

    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt = new Date();

    private String authorDisplayName;
    private String authorUserName;

    @JsonIgnore
    @ManyToOne
    private Topic topic;

    @JsonIgnore
    @ManyToOne
    private PublicUser author;

    @JsonIgnore
    public int getVotesCount(){
        return votes.size();
    }
}