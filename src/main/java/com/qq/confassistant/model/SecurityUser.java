package com.qq.confassistant.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.*;

@Entity
@Table(name = "security_user")
@Getter
@Setter
@NoArgsConstructor
public class SecurityUser implements UserDetails {

    @Id
    @NotNull(message = "{validation.user.username.notNull}")
    @NotEmpty(message = "{validation.user.username.notEmpty}")
    private String username;

    @Column(unique = true)
    private String email;

    @NotNull(message = "{validation.user.displayName.notNull}")
    @NotEmpty(message = "{validation.user.displayName.notEmpty}")
    private String displayName;

    @NotNull(message = "{validation.user.password.notNull}")
    @NotEmpty(message = "{validation.user.password.notEmpty}")
    private String password;
    private String image;
    @Enumerated(EnumType.STRING)
    private Role role = Role.ROLE_USER;

    private boolean enabled = true;

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return Collections.singletonList(new SimpleGrantedAuthority(role.name()));
    }

    @JsonIgnore
    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @JsonIgnore
    @Override
    public boolean isAccountNonLocked() {
        return enabled;
    }

    @JsonIgnore
    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SecurityUser user = (SecurityUser) o;
        return Objects.equals(username, user.username);
    }

    @Override
    public int hashCode() {

        return Objects.hash(username);
    }
}
