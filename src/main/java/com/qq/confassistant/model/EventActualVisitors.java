package com.qq.confassistant.model;

import com.qq.confassistant.id.EventPublicUserId;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
@Getter
@Setter
@EqualsAndHashCode
@NoArgsConstructor
public class EventActualVisitors {

    @EmbeddedId
    @AttributeOverrides({
            @AttributeOverride(name = "username", column = @Column(name = "actual_visitors_username")),
    })
    EventPublicUserId eventPublicUserId;

    @ManyToOne()
    @JoinColumn( insertable=false, updatable=false)
    Event event;

    @ManyToOne
    @JoinColumn(name = "actual_visitors_username", insertable=false, updatable=false)
    PublicUser user;

    public EventActualVisitors(Event event, PublicUser publicUser){
        this.event = event;
        this.user = publicUser;
        this.eventPublicUserId = new EventPublicUserId(event.getId(), publicUser.getUsername());

    }
}
