package com.qq.confassistant.model;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
@EqualsAndHashCode
@ToString(exclude = {"tagRelations"})
public class Tag {
    @Id
    @GeneratedValue
    Long id;
    String name;


    @JsonIgnoreProperties({"tagRelations", "tagRoot"})
    @ManyToMany
    @JoinTable(name = "tag_relations",
            joinColumns = @JoinColumn(name = "tag_id"),
            inverseJoinColumns = @JoinColumn(name = "tag_relations_id"))
    List<Tag> tagRelations = new ArrayList<>();

}
