package com.qq.confassistant.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;

@Getter @Setter @AllArgsConstructor
public class ErrorInfo {

    private HttpStatus status;
    private String name;
    private String message;
    private String[] errors;
}
