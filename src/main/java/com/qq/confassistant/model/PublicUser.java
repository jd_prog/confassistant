package com.qq.confassistant.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "security_user")
@Getter
@Setter
@EqualsAndHashCode
@NoArgsConstructor
public class PublicUser {

    @Id
    @NotNull(message = "{validation.user.username.notNull}")
    @NotEmpty(message = "{validation.user.username.notEmpty}")
    private String username;
    private String image;

    @Column(unique = true)
    private String email;

    @NotNull(message = "{validation.user.displayName.notNull}")
    @NotEmpty(message = "{validation.user.displayName.notEmpty}")
    private String displayName;


}
