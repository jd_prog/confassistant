package com.qq.confassistant.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.mysql.jdbc.Blob;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

@Entity
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value={"attachment"})
public class Attachment {

    @Id
    @GeneratedValue
    private long id;
    @NotNull(message = "{validation.attachment.name.notNull")
    @NotEmpty(message = "{validation.attachment.name.notEmpty")
    private String name;
    @NotNull(message = "{validation.attachment.type.notNull")
    @NotEmpty(message = "{validation.attachment.type.notEmpty")
    private String type;
    private String downloadUri;
    @NotNull(message = "{validation.attachment.attachment.notNull")
    @NotEmpty(message = "{validation.attachment.attachment.notEmpty")
    private byte[] attachment;

}
