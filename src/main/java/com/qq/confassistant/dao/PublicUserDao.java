package com.qq.confassistant.dao;

import com.qq.confassistant.model.PublicUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.NamedQuery;
import java.util.List;

@Repository
public interface PublicUserDao extends JpaRepository<PublicUser, String> {
    @Query(value = "select u.username, u.display_name, u.email, u.image from event e join stream s on e.id = s.event_id join topic t on s.id = t.stream_id join topic_users t2 on t.id = t2.topic_id join security_user u on t2.users_username = u.username where e.id =?1", nativeQuery = true)
    Iterable<PublicUser> findAllByEvent(long eventId);
    @Query(value = "select u.username, u.display_name, u.email, u.image from topic_users t2 join security_user u on t2.users_username = u.username where t2.topic_id =?1", nativeQuery = true)
    Iterable<PublicUser> getAllByTopicId(long topicId);
}
