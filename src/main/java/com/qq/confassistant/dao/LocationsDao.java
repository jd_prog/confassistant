package com.qq.confassistant.dao;

import com.qq.confassistant.model.Location;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by yana on 16.04.18.
 */
public interface LocationsDao extends CrudRepository<Location, Long> {
}
