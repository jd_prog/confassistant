package com.qq.confassistant.dao;

import com.qq.confassistant.model.PublicUser;
import com.qq.confassistant.model.Stream;
import com.qq.confassistant.model.Topic;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;

/**
 * Created by yana on 12.04.18.
 * <p>
 * create
 * update
 * delete
 * getById
 * getAllByEvent (with params)
 * getAllActive (useless if there will be search with params (getAll))
 * getAllByEventAndSpeaker
 * getAllByStream
 */
@Repository
public interface TopicsDao extends CrudRepository<Topic, Long>{

    @Query("select t from Topic t where t.stream = :stream")
    List<Topic> findTopicsByStream(@Param("stream") Stream stream);
}
