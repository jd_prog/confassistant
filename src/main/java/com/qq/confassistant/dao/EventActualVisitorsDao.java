package com.qq.confassistant.dao;

import com.qq.confassistant.id.EventPublicUserId;
import com.qq.confassistant.model.Event;
import com.qq.confassistant.model.EventActualVisitors;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
public interface EventActualVisitorsDao extends JpaRepository <EventActualVisitors, EventPublicUserId> {

    List<EventActualVisitors> findAllByEvent(Event event);

    @Modifying
    @Transactional
    @Query(value = "delete from event_actual_visitors  where event_id = ?1", nativeQuery = true)
    void deleteEventActualVisitorsByEvent_Id(Long event_id);
}
