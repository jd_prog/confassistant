package com.qq.confassistant.dao;

import com.qq.confassistant.model.Event;
import com.qq.confassistant.model.Stream;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by yana on 13.04.18.
 */
public interface StreamsDao extends CrudRepository<Stream, Long> {

    @Query("select s from Stream s where s.event = :event")
    List<Stream> findStreamsByEvent(@Param("event") Event event);
}
