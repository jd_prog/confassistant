package com.qq.confassistant.dao;

import com.qq.confassistant.model.Attachment;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AttachmentDao extends JpaRepository<Attachment, Long> {
}
