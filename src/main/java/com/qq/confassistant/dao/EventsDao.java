package com.qq.confassistant.dao;

import com.qq.confassistant.model.*;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by yana on 12.04.18.
 */
@Repository
public interface EventsDao extends JpaRepository<Event, Long> {

    List<Event> findEventsByStatus(EventStatus status);

    List<Event> findAllByCreatorUsername(String username);

    List<Event> findAllByTags(Tag tag);
}
