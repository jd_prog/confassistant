package com.qq.confassistant.dao;

import com.qq.confassistant.model.SecurityUser;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by yana on 13.04.18.
 */
@Repository
public interface SecurityUserDao extends CrudRepository<SecurityUser, String> {
}
