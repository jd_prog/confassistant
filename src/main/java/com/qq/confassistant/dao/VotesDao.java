package com.qq.confassistant.dao;

import com.qq.confassistant.model.Question;
import com.qq.confassistant.model.Vote;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface VotesDao extends CrudRepository<Vote, Long> {

    int countAllByQuestionIdAndUsername(long questionId, String username);

    void deleteAllByQuestionAndUsername(Question question, String username);

    int countByQuestionId(long questionId);
}
