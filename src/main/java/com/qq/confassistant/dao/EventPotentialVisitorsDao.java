package com.qq.confassistant.dao;

import com.qq.confassistant.id.EventPublicUserId;
import com.qq.confassistant.model.Event;
import com.qq.confassistant.model.EventPotentialVisitors;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
public interface EventPotentialVisitorsDao extends JpaRepository<EventPotentialVisitors, EventPublicUserId> {

    List<EventPotentialVisitors> findAllByEvent(Event event);


    @Modifying
    @Transactional
    @Query(value = "delete from event_potential_visitors  where event_id = ?1", nativeQuery = true)
    void deleteEventPotentialVisitorsByEvent_Id(Long event_id);
}
