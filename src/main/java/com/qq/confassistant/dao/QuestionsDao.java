package com.qq.confassistant.dao;

import com.qq.confassistant.model.Question;
import com.qq.confassistant.model.QuestionStatus;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.Collection;

/**
 * Created by yana on 12.04.18.
 */
public interface QuestionsDao extends CrudRepository<Question, Long> {

    @Query("select q from Question q left join q.topic t where t.id = ?1")
    Collection<Question> findAllByTopic(long topicId);

    Collection<Question> findAllByQuestionStatus(QuestionStatus status);

    int countAllByIdAndAuthorUsername(long id, String username);
}
