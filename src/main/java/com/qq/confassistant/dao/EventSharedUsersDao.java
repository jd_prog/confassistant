package com.qq.confassistant.dao;

import com.qq.confassistant.id.EventPublicUserId;
import com.qq.confassistant.model.Event;
import com.qq.confassistant.model.EventSharedUsers;
import com.qq.confassistant.model.PublicUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
public interface EventSharedUsersDao extends JpaRepository <EventSharedUsers, EventPublicUserId> {

    List<EventSharedUsers> findAllByEvent(Event event);

    List<EventSharedUsers> findAllByUser(PublicUser publicUser);

    @Modifying
    @Transactional
    @Query(value = "delete from event_shared_users  where event_id = ?1", nativeQuery = true)
    void deleteEventSharedUsersByEvent_Id(Long id);

}
