package com.qq.confassistant.dao;

import com.qq.confassistant.model.Tag;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Collection;
import java.util.List;

public interface TagDao extends JpaRepository<Tag, Long> {

    List<Tag> findByNameIn(Collection<String> names);

    Tag getTagByName(String name);

}
