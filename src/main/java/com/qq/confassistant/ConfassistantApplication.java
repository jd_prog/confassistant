package com.qq.confassistant;

import com.qq.confassistant.model.Role;
import com.qq.confassistant.model.SecurityUser;
import com.qq.confassistant.service.IUserService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@SpringBootApplication
public class ConfassistantApplication {

    public static void main(String[] args) {
        SpringApplication.run(ConfassistantApplication.class, args);
    }

    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    CommandLineRunner init(IUserService userService) {

        return (evt) -> {
            userService.save(createBaseUser());
        };
    }


    private SecurityUser createBaseUser() {
        SecurityUser user = new SecurityUser();
        user.setEnabled(true);
        user.setPassword("secret");
        user.setUsername("confassistant");
        user.setDisplayName("confassistant");
        user.setRole(Role.ROLE_BASIC);

        return user;
    }


}




