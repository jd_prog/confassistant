package com.qq.confassistant.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.error.OAuth2AccessDeniedHandler;

/**
 * GET     /users/me                  - autentificated
 * GET     /users                     - ADMIN
 * POST    /users/register            - Basic Auth
 * <p>
 * <p>
 * GET     /api/events               - authentificated
 * GET     /api/events/{id}          - authentificated
 * POST    /api/events               - ADMIN
 * PUT     /api/events               - ADMIN
 * DELETE  /api/events/{id}          - ADMIN
 * <p>
 * <p>
 * GET     /api/topics/{topicId}/questions    - authentificated
 * GET     /api/questions/{questionId}        - authentificated
 * POST    /api/topics/{topicId}/questions    - authentificated
 * PUT     /api/topics/{topicId}/questions    - authentificated
 * POST    /api/questions/{questionId}/open   - authentificated
 * POST    /api/questions/{questionId}/close  - authentificated
 * POST    /api/questions/{questionId}/vote   - authentificated
 * DELETE  /api/questions/{questionId}        - ADMIN
 * <p>
 * <p>
 * GET     /api/speakers                      - authentificated
 * GET     /api/events/{eventId}/speakers     - authentificated
 * GET     /api/speakers/{id}                 - autentificated
 * POST    /api/speakers                      - ADMIN
 * PUT     /api/speakers                      - ADMIN
 * DELETE  /api/speakers/{id}                 - ADMIN
 * <p>
 * <p>
 * GET     /api/streams/{id}                 - authentificated
 * GET     /api/events/{eventId}/streams     - authentificated
 * POST    /api/streams                      - ADMIN
 * PUT     /api/streams                      - ADMIN
 * DELETE  /api/streams/{id}                 - ADMIN
 * <p>
 * <p>
 * GET     /api/events/{eventId}/topics          - authentificated
 * GET     /api/streams/{streamId}/topics        - authentificated
 * GET     /api/topics/{topicId}                 - authentificated
 * POST    /api/events/{eventId}/topics          - ADMIN
 * PUT     /api/events/{eventId}/topics          - ADMIN
 * DELETE  /api/topics/{topicId}                 - ADMIN
 */
@Configuration
@EnableResourceServer
public class ResourceServerConfig extends ResourceServerConfigurerAdapter {

    private static final String RESOURCE_ID = "resource_id";

    @Override
    public void configure(ResourceServerSecurityConfigurer resources) {
        resources.resourceId(RESOURCE_ID).stateless(false);
    }

    @Override
    public void configure(HttpSecurity http) throws Exception {
        http
//                .anonymous().disable()
                .authorizeRequests()
                .antMatchers(HttpMethod.GET, "/api/server/").authenticated()
                .antMatchers(HttpMethod.GET, "/api/events/**/qrcode/").anonymous()
                .antMatchers(HttpMethod.GET, "/api/attachment/download/**/**").permitAll()
                .antMatchers(HttpMethod.GET, "/api/**").authenticated()
                .antMatchers(HttpMethod.OPTIONS, "/oauth/**").permitAll()

                //users
                .antMatchers(HttpMethod.GET, "/users/me").authenticated()
                .antMatchers(HttpMethod.GET, "/users**").hasRole("ADMIN")
                .antMatchers(HttpMethod.POST, "/users/register/admin/**").permitAll()
                .antMatchers(HttpMethod.POST, "/users/**").permitAll()

                //tags
                .antMatchers(HttpMethod.GET, "/api/tags").authenticated()
                .antMatchers(HttpMethod.POST, "/api/tags").authenticated()

                //events
                .antMatchers(HttpMethod.GET, "/api/events**").authenticated()
                .antMatchers(HttpMethod.GET, "/api/events/**/**/**").authenticated()
                .antMatchers(HttpMethod.POST, "/api/events**").hasRole("ADMIN")
                .antMatchers(HttpMethod.POST, "/api/events/**/attachments/**").hasRole("ADMIN")
                .antMatchers(HttpMethod.DELETE, "/api/events/**/attachment/**").hasRole("ADMIN")
                .antMatchers(HttpMethod.POST, "/api/events/**/**").authenticated()
                .antMatchers(HttpMethod.PUT, "/api/events**").hasRole("ADMIN")
                .antMatchers(HttpMethod.DELETE, "/api/events/**").hasRole("ADMIN")

                .antMatchers(HttpMethod.POST, "/api/events/filter/tags").anonymous()
                .antMatchers(HttpMethod.POST, "/api/events/filter/tag").anonymous()

                //topics
                .antMatchers(HttpMethod.GET, "/api/topics**").authenticated()
                .antMatchers(HttpMethod.GET, "/api/topics/**/questions").authenticated()
                .antMatchers(HttpMethod.POST, "/api/streams/**/topics**").hasRole("ADMIN")
                .antMatchers(HttpMethod.PUT, "/api/streams/**/topics**").hasRole("ADMIN")
                .antMatchers(HttpMethod.DELETE, "/api/topics/**").hasRole("ADMIN")

                //questions
                .antMatchers(HttpMethod.GET, "/api/questions/**").authenticated()
                .antMatchers(HttpMethod.POST, "/api/questions/**").authenticated()
                .antMatchers(HttpMethod.POST, "/api/topics/**/questions**").authenticated()
                .antMatchers(HttpMethod.PUT, "/api/topics/**/questions").authenticated()
                .antMatchers(HttpMethod.DELETE, "/api/questions/**").authenticated()

                //speakers
                .antMatchers(HttpMethod.GET, "/api/speakers**").authenticated()
                .antMatchers(HttpMethod.GET, "/api/events/**/speakers").authenticated()
                .antMatchers(HttpMethod.GET, "/api/topics/**/speakers").authenticated()
                .antMatchers(HttpMethod.POST, "/api/speakers**").hasRole("ADMIN")
                .antMatchers(HttpMethod.PUT, "/api/speakers**").hasRole("ADMIN")
                .antMatchers(HttpMethod.DELETE, "/api/speakers/**").hasRole("ADMIN")

                //streams
                .antMatchers(HttpMethod.GET, "/api/streams**").authenticated()
                .antMatchers(HttpMethod.GET, "/api/streams/**/topics").authenticated()
                .antMatchers(HttpMethod.GET, "/api/events/**/streams**").authenticated()
                .antMatchers(HttpMethod.POST, "/api/events/**/streams**").hasRole("ADMIN")
                .antMatchers(HttpMethod.PUT, "/api/events/**/streams**").hasRole("ADMIN")
                .antMatchers(HttpMethod.DELETE, "/api/streams/**").hasRole("ADMIN")
                //attachment
                .antMatchers(HttpMethod.POST, "api/attachment/add").hasRole("ADMIN")
                .antMatchers(HttpMethod.DELETE, "api/attachment/**").hasRole("ADMIN")


                .and().exceptionHandling().accessDeniedHandler(new OAuth2AccessDeniedHandler())

                .and().httpBasic();


    }
}