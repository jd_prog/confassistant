package com.qq.confassistant.config;

import com.qq.confassistant.dao.StreamsDao;
import com.qq.confassistant.dao.TopicsDao;
import com.qq.confassistant.model.Stream;
import com.qq.confassistant.model.Topic;
import com.qq.confassistant.validation.validator.EntityExistsValidator;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;

@Configuration
public class Validators {

    @Bean
    public EntityExistsValidator<Stream, Long> streamValidator(StreamsDao streamsDao) {
        return new EntityExistsValidator<>(streamsDao);
    }

    @Bean
    public EntityExistsValidator<Topic, Long> topicValidator(TopicsDao topicsDao) {
        return new EntityExistsValidator<>(topicsDao);
    }

    @Bean
    public MessageSource messageSource() {
        ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
        messageSource.setBasename("classpath:messages");
        messageSource.setDefaultEncoding("UTF-8");
        return messageSource;
    }

    @Bean
    public LocalValidatorFactoryBean validator() {
        LocalValidatorFactoryBean bean = new LocalValidatorFactoryBean();
        bean.setValidationMessageSource(messageSource());
        return bean;
    }
}
