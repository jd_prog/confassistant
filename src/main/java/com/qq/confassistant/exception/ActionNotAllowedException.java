package com.qq.confassistant.exception;

import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@Getter
@Setter
@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class ActionNotAllowedException extends RuntimeException {

    private String action;

    public ActionNotAllowedException(String action, String message) {
        super(message);
        this.action = action;
    }
}
