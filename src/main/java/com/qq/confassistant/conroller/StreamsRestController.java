package com.qq.confassistant.conroller;

import com.qq.confassistant.model.Stream;
import com.qq.confassistant.service.IStreamsService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * Created by yana on 13.04.18.
 */
@CrossOrigin(origins = {"http://localhost:4200",
        "http://localhost:4201",
        "https://confassistant.vitech.com.ua:8443",
        "https://qq.vitech.com.ua:8443/"
})@RestController
@RequestMapping(value = "/api/")
@AllArgsConstructor
public class StreamsRestController {

    private IStreamsService streamsService;

    @GetMapping(path = "streams/{id}")
    public Stream get(@PathVariable Long id){
        return streamsService.get(id);
    }

    @GetMapping(path = "/events/{eventId}/streams")
    public List<Stream> getStreamsByEvent(@PathVariable Long eventId){
        return streamsService.getStreamsByEvent(eventId);
    }


    @PostMapping(path = "/events/{eventId}/streams")
    public Stream create(@PathVariable Long eventId,
                         @Valid @RequestBody Stream stream){
        return streamsService.create(eventId, stream);
    }

    @PutMapping(path = "/events/{eventId}/streams")
    public Stream update(@Valid @RequestBody Stream stream){
        return streamsService.update(stream);
    }

    @DeleteMapping(path = "streams/{id}")
    public void delete(@PathVariable Long id){
        streamsService.delete(id);
    }

}
