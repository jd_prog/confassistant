package com.qq.confassistant.conroller;

import com.qq.confassistant.model.Event;
import com.qq.confassistant.model.PublicUser;
import com.qq.confassistant.model.SecurityUser;
import com.qq.confassistant.service.impl.UsersService;
import lombok.AllArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

/**
 * Created by yana on 13.04.18.
 */
@CrossOrigin(origins = {"http://localhost:4200",
        "http://localhost:4201",
        "https://confassistant.vitech.com.ua:8443",
        "https://qq.vitech.com.ua:8443/"
})
@RestController
@RequestMapping(value = "/users")
@AllArgsConstructor
public class UsersController {

    private UsersService usersService;

    @GetMapping(path = "/me")
    public UserDetails getUser(Principal principal) {

        return usersService.loadUserByUsername(principal.getName());
    }

    @GetMapping()
    public Iterable<SecurityUser> getAll() {
        return usersService.getAll();
    }

    @PostMapping(path = "/register")
    public SecurityUser save(@RequestBody SecurityUser user) {
        return this.usersService.save(user);
    }

    @PostMapping(path = "/register/admin")
    public SecurityUser saveAdmin(@RequestBody SecurityUser user) {
        return this.usersService.saveAsAdmin(user);
    }

    @GetMapping(path = "/events/{eventId}/speakers")
    public Iterable<PublicUser> getAllByEventId(@PathVariable Long eventId) {
        return usersService.getAllByEventId(eventId);
    }

    @GetMapping(path = "/topics/{topicId}/speakers")
    public Iterable<PublicUser> getAllByTopicId(@PathVariable Long topicId) {
        return usersService.getAllByTopicId(topicId);
    }

    @GetMapping(path = "/public")
    public Iterable<PublicUser> getAllPublicUsers() {
        return usersService.getAllPublicUsers();
    }


    @GetMapping(path = "/events/{id}/all")
    public Iterable<PublicUser> getEvent(@PathVariable Long id) {
        return usersService.allUsersByEvent(id);
    }

}
