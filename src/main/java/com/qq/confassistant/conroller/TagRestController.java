package com.qq.confassistant.conroller;

import com.qq.confassistant.model.Tag;
import com.qq.confassistant.service.ITagService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = {"http://localhost:4200",
        "http://localhost:4201",
        "https://confassistant.vitech.com.ua:8443",
        "https://qq.vitech.com.ua:8443/"
})@RestController
@RequestMapping(value = "/api/")
@AllArgsConstructor
public class TagRestController {

    final ITagService tagService;


    @GetMapping("tags")
    List<Tag> getAll(){
        return tagService.getAll();
    }

    @GetMapping("tags/{id}")
    Tag getById(@PathVariable Long id){
        return tagService.getById(id);
    }

    @PostMapping("tags")
    Tag create(@RequestBody Tag tag) {
        return tagService.create(tag);
    }

    @PutMapping("tags/{rootTagId}/{tagId")
    Tag mapTagToAnotherTag(@PathVariable Long rootTagId, @PathVariable Long tagId){
        return tagService.mapTagToAnotherTag(rootTagId, tagId);
    }

    @PutMapping("events/{eventId}/tags/{tagId}")
    Tag addEvent(@PathVariable Long eventId,@PathVariable Long tagId){
        return tagService.addEvent(eventId, tagId);
    }

    @DeleteMapping("events/{eventId}/tags/{tagId}")
    void removeFromEvent(@PathVariable Long eventId, @PathVariable Long tagId){
        this.tagService.removeFromEvent(eventId, tagId);
    }

}
