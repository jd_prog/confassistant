package com.qq.confassistant.conroller;

import com.qq.confassistant.model.Topic;
import com.qq.confassistant.service.ITopicsService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Collection;

/**
 * Created by yana on 12.04.18.
 */

@CrossOrigin(origins = {"http://localhost:4200",
        "http://localhost:4201",
        "https://confassistant.vitech.com.ua:8443",
        "https://qq.vitech.com.ua:8443/"
})@RestController
@RequestMapping(value = "/api/")
@AllArgsConstructor
public class TopicsRestController {

    private ITopicsService topicsService;

    @GetMapping(path = "/streams/{streamId}/topics")
    public Collection<Topic> getTopicsByStreamId(@PathVariable Long streamId) {
        return topicsService.getAllByStreamId(streamId);
    }

    @GetMapping(path = "topics/{topicId}")
    public Topic get(@PathVariable Long topicId) {
        return topicsService.get(topicId);
    }


    @PostMapping(path = "streams/{streamId}/topics")
    public Topic create(@PathVariable Long streamId,
                        @Valid @RequestBody Topic topic) {
        return topicsService.create(streamId, topic);
    }

    @PutMapping(path = "streams/{streamId}/topics")
    public Topic update(@PathVariable Long streamId,
                        @Valid  @RequestBody Topic topic) {
        return topicsService.update(streamId, topic);
    }

    @DeleteMapping(path = "topics/{topicId}")
    public void delete(@PathVariable Long topicId) {
        topicsService.delete(topicId);
    }
}

