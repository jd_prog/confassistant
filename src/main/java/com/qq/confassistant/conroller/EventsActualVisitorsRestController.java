package com.qq.confassistant.conroller;

import com.qq.confassistant.model.PublicUser;
import com.qq.confassistant.service.impl.ActualVisitorsService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@CrossOrigin(origins = {"http://localhost:4200",
        "http://localhost:4201",
        "https://confassistant.vitech.com.ua:8443",
        "https://qq.vitech.com.ua:8443/"
})@RestController
@RequestMapping(value = "/api/")
@AllArgsConstructor
public class EventsActualVisitorsRestController {

    @Autowired
    ActualVisitorsService actualVisitorsService;


    @PostMapping(path = "events/{id}/actual/visitors/add")
    PublicUser addEventActualVisitors(@PathVariable Long id, @RequestBody PublicUser publicUser) {
        return actualVisitorsService.create(id, publicUser.getUsername());
    }


    @GetMapping(path = "events/{id}/actual/visitors")
    List<PublicUser> getAllEventVisitors(@PathVariable Long id) {
        return actualVisitorsService.getActualVisitorsByEvent(id);
    }

}
