package com.qq.confassistant.conroller;


import com.qq.confassistant.model.PublicUser;
import com.qq.confassistant.service.impl.PotentialVisitorsService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = {"http://localhost:4200",
        "http://localhost:4201",
        "https://confassistant.vitech.com.ua:8443",
        "https://qq.vitech.com.ua:8443/"
})@RestController
@RequestMapping(value = "/api/")
@AllArgsConstructor
public class EventsPotentialVisitorsRestController {

    @Autowired
    PotentialVisitorsService potentialVisitorsService;

    @GetMapping(path = "events/{id}/potential/visitors")
    List<PublicUser> getAllPotentialVisitors(@PathVariable Long id){
        return potentialVisitorsService.getAllPotentialVisitorsByEvent(id);
    }

    @PostMapping(path = "events/{id}/potential/visitors/add")
    PublicUser addPotentialVisitor(@PathVariable Long id, @RequestBody PublicUser publicUser){
        return potentialVisitorsService.create(id, publicUser.getUsername());
    }

    @PostMapping(path = "events/{id}/potential/visitors/remove")
    PublicUser removePotentialVisitors(@PathVariable Long id, @RequestBody PublicUser publicUser){
        return potentialVisitorsService.remove(id, publicUser.getUsername());
    }

}
