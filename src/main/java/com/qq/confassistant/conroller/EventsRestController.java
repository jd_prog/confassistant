package com.qq.confassistant.conroller;

import com.qq.confassistant.model.Attachment;
import com.qq.confassistant.model.Event;
import com.qq.confassistant.service.IEventService;
import com.qq.confassistant.service.IQRService;
import lombok.AllArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;


import javax.servlet.annotation.MultipartConfig;
import javax.validation.Valid;
import java.util.List;
import java.util.Set;

/**
 * Created by yana on 12.04.18.
 */
@CrossOrigin(origins = {"http://localhost:4200",
        "http://localhost:4201",
        "https://confassistant.vitech.com.ua:8443",
        "https://qq.vitech.com.ua:8443/"
})@RestController
@RequestMapping(value = "/api/")
@AllArgsConstructor
public class EventsRestController {

    private IEventService eventService;
    private IQRService qrService;


    @GetMapping(path = "events")
    public Iterable<Event> getEvents() {
        return eventService.getAll();
    }


    @GetMapping(path = "users/{username}/events")
    public Iterable<Event> getEventsCreatedBy(@PathVariable String username) {
        return eventService.getAllCreatedBy(username);
    }

    @GetMapping(path = "events/published")
    public Iterable<Event> getPublishedEvents() {
        return eventService.getPublished();
    }


    @GetMapping(path = "events/{id}")
    public Event getEvent(@PathVariable Long id) {
        return eventService.get(id);
    }

    @PostMapping(path = "events")
    public Event create(@Valid @RequestBody Event event) {

        return eventService.create(event);
    }

    @DeleteMapping(path = "events/{id}")
    public void delete(@PathVariable Long id) {
        eventService.delete(id);
    }

    @PutMapping(path = "events")
    public Event update(@Valid @RequestBody Event event) {
        return eventService.update(event);
    }

    @PostMapping(path = "events/{id}")
    public Event changeStatus(@PathVariable Long id) {
        return eventService.changeStatusToPublish(id);
    }


    @GetMapping(path = "events/{id}/qrcode", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    public @ResponseBody
    byte[] getevetntQrCode(@PathVariable Long id) throws Exception {
        String host = "events/" + id + "/add/actualvisitor";
        return qrService.getQRCodeImage(host, 500, 500);
    }


    @GetMapping(path = "events/filter/tags/{tagId}")
    public Set<Event> getByTagId(@PathVariable Long tagId) {
        return eventService.findByTagId(tagId);
    }

    @PostMapping(path = "events/filter/tags")
    public Set<Event> getByTextWithTags(@RequestBody String text) {
        return eventService.findByTextWithTags(text);
    }

    @PostMapping(path = "events/filter/tag")
    public List<Event> getByTagNameIn(@RequestBody String name) {
        return eventService.findByTagNameIn(name);
    }

    @PutMapping(path = "/events/{eventId}/attachments/{attachmentsId}")

    public Event addAttachmentsToEvent(@PathVariable Long eventId, @PathVariable Long attachmentsId) {
        return eventService.addAttachmentsToEvent(eventId, attachmentsId);
    }

    @DeleteMapping(path = "/events/{eventId}/attachment/{attachmentId}")
    public Event deleteAttachment(@PathVariable Long eventId, @PathVariable Long attachmentId) {
        return eventService.deleteAttachment(eventId, attachmentId);
    }

    @PostMapping(path = "events/grabed")
    public List<Event> saveGrabed(@RequestBody List<Event> grabed) {
        return this.eventService.saveGrabed(grabed);
    }


}
