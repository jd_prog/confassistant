package com.qq.confassistant.conroller;

import com.qq.confassistant.model.Event;
import com.qq.confassistant.model.PublicUser;
import com.qq.confassistant.service.IEventSharedUsersService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


import java.util.List;

@CrossOrigin(origins = {"http://localhost:4200",
        "http://localhost:4201",
        "https://confassistant.vitech.com.ua:8443",
        "https://qq.vitech.com.ua:8443/"
})@RestController
@RequestMapping(value = "/api/")
@AllArgsConstructor
public class EventsSharedUsersRestController {

    @Autowired
    IEventSharedUsersService eventSharedUsersServer;



    @GetMapping(path = "events/{id}/share")
    List<PublicUser> getEventSharedUsers(@PathVariable Long id){
        return eventSharedUsersServer.getAllEventSharedUsers(id);
    }

    @GetMapping(path = "{username}/shared")
    List<Event> getSharedEvents(@PathVariable String username){
        return eventSharedUsersServer.getSharedEvents(username);
    }

    @PostMapping(path = "events/{id}/share/add")
    List<PublicUser> shareEventWithUsers(@PathVariable Long id, @RequestBody List<PublicUser> list){
        return eventSharedUsersServer.shareUsers(id,list);
    }

    @PostMapping(path = "events/{id}/share/remove")
    void removeEventSharedUsers(@PathVariable Long id, @RequestBody PublicUser publicUser){
        eventSharedUsersServer.removeUsers(id, publicUser.getUsername());
    }

}
