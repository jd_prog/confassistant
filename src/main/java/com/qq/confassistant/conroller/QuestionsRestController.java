package com.qq.confassistant.conroller;

import com.qq.confassistant.model.Question;
import com.qq.confassistant.model.QuestionStatus;
import com.qq.confassistant.service.IQuestionsService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;

/**
 * Created by yana on 12.04.18.
 */
@CrossOrigin(origins = {"http://localhost:4200",
        "http://localhost:4201",
        "https://confassistant.vitech.com.ua:8443",
        "https://qq.vitech.com.ua:8443/"
})@RestController
@RequestMapping(value = "/api/")
@AllArgsConstructor
public class QuestionsRestController {

    private IQuestionsService questionsService;

    @GetMapping(path = "topics/{topicId}/questions")
    public Iterable<Question> getAllByTopic(@PathVariable Long topicId) {
        return questionsService.getAllByTopic(topicId);
    }

    @GetMapping(path = "questions/{questionId}")
    public Question get(@PathVariable Long questionId) {
        return questionsService.get(questionId);
    }

    @PostMapping(path = "topics/{topicId}/questions")
    public Question create(@PathVariable Long topicId,
                           @Valid @RequestBody Question question,
                           Principal principal) {
        return questionsService.create(topicId, principal.getName(), question);
    }

    @PutMapping(path = "topics/{topicId}/questions")
    public Question update(
            @PathVariable Long topicId,
            @Valid @RequestBody Question question) {
        return questionsService.update(topicId, question);
    }

    @PostMapping(path = "questions/{questionId}/open")
    public Question open(
            @PathVariable Long questionId) {
        return questionsService.changeStatus(questionId, QuestionStatus.OPENED);
    }

    @PostMapping(path = "questions/{questionId}/close")
    public Question close(
            @PathVariable Long questionId) {
        return questionsService.changeStatus(questionId, QuestionStatus.CLOSED);
    }

    @PostMapping(path = "questions/{questionId}/vote")
    public Question vote(@PathVariable Long questionId,
                                       Principal principal) {

        return questionsService.vote(questionId, principal.getName());
    }

    @PostMapping(path = "questions/{questionId}/unvote")
    public Question removeVote(@PathVariable Long questionId,
                               Principal principal) {
        return questionsService.removeVote(questionId, principal.getName());
    }


    @DeleteMapping(path = "questions/{questionId}")
    public void delete(@PathVariable Long questionId) {
        questionsService.delete(questionId);
    }

}
