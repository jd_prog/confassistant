package com.qq.confassistant.conroller;

import com.qq.confassistant.model.Attachment;
import com.qq.confassistant.service.IAttachmentService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.annotation.MultipartConfig;
import java.io.IOException;

import static org.springframework.http.MediaType.IMAGE_JPEG_VALUE;
import static org.springframework.http.MediaType.IMAGE_PNG_VALUE;

/**
 * Created by yana on 12.04.18.
 */
@CrossOrigin(origins = {"http://localhost:4200",
        "http://localhost:4201",
        "https://confassistant.vitech.com.ua:8443",
        "https://qq.vitech.com.ua:8443/"
})
@RestController
@MultipartConfig(maxFileSize = 10737418240L, maxRequestSize = 10737418240L, fileSizeThreshold = 52428800)
@RequestMapping(value = "/api/")
@AllArgsConstructor
public class AttachmentRestController {
    private IAttachmentService attachmentService;

    @PostMapping(path = "/attachment/add")
    public Attachment attachment(@RequestParam("attachment") MultipartFile attachment) throws IOException {
        return attachmentService.create(attachment);
    }
    @GetMapping(path = "/attachment/download/{id}/{fileName:.+}", produces = IMAGE_JPEG_VALUE)
    public @ResponseBody
    byte[] getJpeg(@PathVariable Long id) {
        return attachmentService.findAttachmentById(id);
    }

    @GetMapping(path = "/attachment/download/{id}/{fileName:.+}", produces = IMAGE_PNG_VALUE)
    public @ResponseBody
    byte[] getPng(@PathVariable Long id) {
        return attachmentService.findAttachmentById(id);
    }
    @GetMapping(path = "get")
    public @ResponseBody
    String get() {
        return attachmentService.get();
    }

}
