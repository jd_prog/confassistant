#ConfassistantApp
Backend part of ConfassistantApp, which contains implementation of REST services to manage events and related data.

Spring Security and Oath2 used to manage users access to data.
User with ROLE_ADMIN has full access to REST services.
User with ROLE_USER has read access to all services, and can perform CRUD operations qith questions only.

Application hoster on Heroku: *https://confassistant-backend-app.herokuapp.com*

In development MySql database user, but for some reasons Heroku allows PostgreSQL database, so, application has two profiles (dev, prod) to work with local and remote database respectively.
Its configurations you can find in application-dev.properties and application-prod.properties.

Application contains maven profiles with plugins to clear and/of fill with some dummy data local (MySql) and remote (PostgreSQL) databases

##Development server
Create local database:

- sudo mysql --password
- create database events_db; 
- create user 'events_admin'@'localhost' identified by 'admin';
- grant all on events_db.* to 'events_admin'@'localhost';

Use **mvn spring-boot:run** to start application. 

##Technology on project
- Sping Boot
- Spring Security
- Hibernate 

##CI
Bitbucket pipelines used to manage deployment processes.
Every time when developer pushes new changes to his branch, pipeline builds the app and runs tests.
When developer pushes changes to master, and tests runs successfully, app is pushed to Heroku repository.

##Routing
- **GET**     /users/me                  
    - autentificated
- **GET**     /users                     
    - ADMIN
- **POST**    /users/register            
    - Basic Auth
- **GET**     /api/events              
    - authentificated
- **GET**     /api/events/{id}          
    - authentificated
- **POST**    /api/events              
    - ADMIN
- **PUT**     /api/events               
    - ADMIN
- **DELETE**  /api/events/{id}          
    - ADMIN
- **GET**     /api/topics/{topicId}/questions    
    - authentificated
- **GET**     /api/questions/{questionId}        
    - authentificated
- **POST**    /api/topics/{topicId}/questions    
    - authentificated
- **PUT**     /api/topics/{topicId}/questions    
    - authentificated
- **POST**    /api/questions/{questionId}/open   
    - authentificated
- **POST**    /api/questions/{questionId}/close  
    - authentificated
- **POST**    /api/questions/{questionId}/vote   
    - authentificated
- **DELETE**  /api/questions/{questionId}        
    - ADMIN
- **GET**     /api/speakers                      
    - authentificated
- **GET**     /api/events/{eventId}/speakers     
    - authentificated
- **GET**     /api/speakers/{id}                 
    - autentificated
- **POST**    /api/speakers                      
    - ADMIN
- **PUT**     /api/speakers                      
    - ADMIN
- **DELETE**  /api/speakers/{id}                 
    - ADMIN
- **GET**     /api/streams/{id}                 
    - authentificated
- **GET**     /api/events/{eventId}/streams     
    - authentificated
- **POST**    /api/streams                      
    - ADMIN
- **PU**T     /api/streams                      
    - ADMIN
- **DELETE**  /api/streams/{id}                 
    - ADMIN
- **GET**     /api/events/{eventId}/topics          
    - authentificated
- **GET**     /api/streams/{streamId}/topics        
    - authentificated
- **GET**     /api/topics/{topicId}                 
    - authentificated
- **POST**    /api/events/{eventId}/topics          
    - ADMIN
- **PUT**     /api/events/{eventId}/topics          
    - ADMIN
- **DELETE**  /api/topics/{topicId}                 
    - ADMIN
