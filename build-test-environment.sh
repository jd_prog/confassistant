db_url="jdbc:postgresql://db-$BITBUCKET_BRANCH:5432/events_db"
timestamp=$(date "+%s")
NUMBER=$(echo $BITBUCKET_BRANCH | tr -dc '0-9')
qq_client="http://qq.vitech.com.ua:$NUMBER"
echo "$timestamp"
mvn_clean="mvn clean"
mvn_package="mvn package"
docker_build="docker build -t confassistant-back-end-$BITBUCKET_BRANCH -f Dockerfile.test --build-arg QQ_CLIENT_URL=$qq_client --build-arg DB_URL=$db_url ."
docker_tag="docker tag confassistant-back-end-$BITBUCKET_BRANCH confassistant-back-end-$BITBUCKET_BRANCH:$timestamp"
docker_push="docker push confassistant-back-end-$BITBUCKET_BRANCH"
eval $mvn_clean
eval $mvn_package
eval $docker_build
eval $docker_tag
eval $docker_push
